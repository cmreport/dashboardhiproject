import { Component } from '@angular/core';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { Router } from '@angular/router'

@Component({
    selector   : 'app-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.scss']
})
export class AppComponent
{
    /**
     * Constructor
     */
    constructor(
        private _router: Router
     )
    {
        // localStorage.removeItem('accessToken');
        // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm92Y29kZSI6IjM0IiwicHJvdm5hbWUiOiLguK3guLjguJrguKXguKPguLLguIrguJjguLLguJnguLUiLCJmdWxsbmFtZSI6IuC4mOC4p-C4seC4iuC4iuC4seC4oiDguYHguKrguIfguYDguJTguLfguK3guJkiLCJpYXQiOjE2NzMzMjg0NTcsImV4cCI6MTcwNDg4NjA1N30.EC5XvMBjmKrTKGT2wfu9vnt0j5SNin5O3xpSCuKhbGA

        if(sessionStorage.getItem('accessToken')){

            const accessToken:any = AuthUtils._decodeToken(sessionStorage.getItem('accessToken'));
            console.log("accessToken");
            sessionStorage.setItem('codewd',accessToken.code)
            sessionStorage.setItem('accesstype',accessToken.is_active)
            console.log(accessToken);
            //this._router.navigate(['/pward']);

        }else{
            this._router.navigate(['/sign-in'])
            // sessionStorage.setItem('accessToken','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXNfbG9naW4iOlt7InVzZXJuYW1lIjoiM2EiLCJmdWxsbmFtZSI6IuC4mOC4p-C4seC4iuC4iuC4seC4oiDguYHguKrguIfguYDguJTguLXguK3guJkiLCJoY29kZSI6IjEwOTU3In1dLCJpYXQiOjE2NzM0MzkxMDIsImV4cCI6MTcwNDk5NjcwMn0.NEZ3JmI-t8PDppoWSXnLvPe2deJ2L5grJKZp5kAPO8g');
        }


    }
}
