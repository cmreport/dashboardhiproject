import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/aipn/aipn.model'

import { AipnService } from '../../../services/aipn.service'


export interface _aipnstatusFlgA {
  an: any;
  hn: any;
  pidpat: any;
  title: any;
  namepat:any;
  dtadm: any;
  dchdate: any;
  leaveday: any;
  dischstat: any;
  dischtype: any;
  admwt: any;
  dischward: any;
  dept: any;
  hospmain: any;
  status_flg: any;
}

export interface _aipnstatusFlgN {
  an: any;
  hn: any;
  pidpat: any;
  title: any;
  namepat:any;
  dtadm: any;
  dchdate: any;
  leaveday: any;
  dischstat: any;
  dischtype: any;
  admwt: any;
  dischward: any;
  dept: any;
  hospmain: any;
  status_flg: any;
}
export interface _aipnstatusFlgC {
  an: any;
  hn: any;
  pidpat: any;
  title: any;
  namepat:any;
  dtadm: any;
  dchdate: any;
  leaveday: any;
  dischstat: any;
  dischtype: any;
  admwt: any;
  dischward: any;
  dept: any;
  hospmain: any;
  status_flg: any;
  checkcode: any;
  cname: any;
  remark: any;

}

export interface _aipnstatusFlgD {
  an: any;
  hn: any;
  pidpat: any;
  title: any;
  namepat:any;
  dtadm: any;
  dchdate: any;
  leaveday: any;
  dischstat: any;
  dischtype: any;
  admwt: any;
  dischward: any;
  dept: any;
  hospmain: any;
  status_flg: any;
  checkcode: any;
  cname: any;
  remark: any;
}

@Component({
  selector: 'app-aipn',
  templateUrl: './aipn.component.html',
  styleUrls: ['./aipn.component.scss']
})
export class AipnComponent {

 //สร้างตัวแปร items,datestart,dateend
//item Aipn statusflg A
itemAipnstatusflgN : any =[];
itemsExeclitemAipnstatusflgN:any = []; 

//item Aipn statusflg A
itemAipnstatusflgA : any =[];
itemsExeclitemAipnstatusflgA:any = []; 

//item Aipn statusflg C
itemAipnstatusflgC : any =[];
itemsExeclitemAipnstatusflgC:any = []; 

//item Aipn statusflg D
itemAipnstatusflgD : any =[];
itemsExeclitemAipnstatusflgD:any = []; 

//get Value Count status
aipn_all_status: any=0;
aipn_statflg_A: any=0;
aipn_statflg_C: any=0;
aipn_statflg_D: any=0;
aipn_statflg_N: any=0;

itemIdpm:any = [];
selecteditemIdpm:any='';

startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');

// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
aipnstatusflgAdisplayedColumns: string[] = ['an','hn','pidpat','title','namepat','dtadm','dchdate','leaveday',
'dischstat','dischtype','admwt','dischward','dept','hospmain','status_flg'];

aipnstatusflgNdisplayedColumns: string[] = ['an','hn','pidpat','title','namepat','dtadm','dchdate','leaveday',
'dischstat','dischtype','admwt','dischward','dept','hospmain','status_flg'];

aipnstatusflgCdisplayedColumns: string[] = ['an','hn','pidpat','title','namepat','dtadm','dchdate','leaveday',
'dischstat','dischtype','admwt','dischward','dept','hospmain','status_flg','checkcode','cname','remark'];

aipnstatusflgDdisplayedColumns: string[] = ['an','hn','pidpat','title','namepat','dtadm','dchdate','leaveday',
'dischstat','dischtype','admwt','dischward','dept','hospmain','status_flg','checkcode','cname','remark'];


@ViewChild(MatPaginator) paginator: MatPaginator;
@ViewChild('paginator2') paginator2: MatPaginator;
@ViewChild('paginator3') paginator3: MatPaginator;
@ViewChild('paginator4') paginator4: MatPaginator;
ngAfterViewInit_aipnstatusflgA() {
    this.itemAipnstatusflgA.paginator = this.paginator;
  }
  ngAfterViewInit_aipnstatusflgC() {
    this.itemAipnstatusflgC.paginator = this.paginator2;
  }
  ngAfterViewInit_aipnstatusflgD() {
    this.itemAipnstatusflgD.paginator = this.paginator3;
  }

  ngAfterViewInit_aipnstatusflgN() {
    this.itemAipnstatusflgN.paginator = this.paginator4;
  }

  constructor(
    private router: Router,
    private aipnService:AipnService,
    private excelService : ExcelService
) {} 

ngOnInit():void{
  this.getLookupDATA();
}

//get DATA Table
async getLookupDATA(){
  await this.getLookupIDPM()
 }
//get DATA Table Cln of dbHI
async getLookupIDPM(){
  let lcIDPM:any = await this.aipnService.select_idpm();
  this.itemIdpm = lcIDPM;
 }



async getaipnInfo(){
  let lcCode:any = this.selecteditemIdpm; //bkk
  let info: any = {
  "ward": lcCode,
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info',info);
  
  //แสดงจำนวนผู้ป่วย APIN
  await this.getaipnallStatusflg(info);
  await this.tab_aipnstatusflgA(info);
  await this.tab_aipnstatusflgC(info);
  await this.tab_aipnstatusflgD(info);
  await this.tab_aipnstatusflgN(info);

}


async getaipnallStatusflg(info){
  try{
    let rs:any = await this.aipnService.getaipn_all_status(info);
    let data:any = rs[0];
    console.log(rs);
    if(rs.length > 0){
      this.aipn_all_status =data.countall;
      this.aipn_statflg_A = data.status_a;
      this.aipn_statflg_C = data.status_c;
      this.aipn_statflg_D = data.status_d;
      this.aipn_statflg_N = data.status_null;
    }    
  }catch(error:any){
    console.log();    
  }
}
async tab_aipnstatusflgA(info){
  this.itemAipnstatusflgA= [];
  this.itemsExeclitemAipnstatusflgA=[];
  try {
      let rs: any = await this.aipnService.getaipn_statusflgA(info);
      let data:any = rs.results.rows;
      console.log('A',rs);
      if (rs.results.ok){
        this.itemsExeclitemAipnstatusflgA = data;
        this.itemAipnstatusflgA = new MatTableDataSource<_aipnstatusFlgA>(data);
        this.ngAfterViewInit_aipnstatusflgA();

      }
    } catch (error: any) {
        console.log();
    }

  }
async tab_aipnstatusflgC(info){
  this.itemAipnstatusflgC= [];
  this.itemsExeclitemAipnstatusflgC=[];
  try {
      let rs: any = await this.aipnService.getaipn_statusflgC(info);
      let data:any = rs.results.rows;
      console.log('C',rs);
      if (rs.results.ok){
        this.itemsExeclitemAipnstatusflgC = data;
        this.itemAipnstatusflgC = new MatTableDataSource<_aipnstatusFlgC>(data);
        this.ngAfterViewInit_aipnstatusflgC();

      }
    } catch (error: any) {
        console.log();
    }

  }

  async tab_aipnstatusflgD(info){
    this.itemAipnstatusflgD= [];
    this.itemsExeclitemAipnstatusflgD=[];
    try {
        let rs: any = await this.aipnService.getaipn_statusflgD(info);
        let data:any = rs.results.rows;
        console.log('D',rs);
        if (rs.results.ok){
          this.itemsExeclitemAipnstatusflgD = data;
          this.itemAipnstatusflgD = new MatTableDataSource<_aipnstatusFlgD>(data);
          this.ngAfterViewInit_aipnstatusflgD();
  
        }
      } catch (error: any) {
          console.log();
      }
  
    }
    async tab_aipnstatusflgN(info){
      this.itemAipnstatusflgN= [];
      this.itemsExeclitemAipnstatusflgN=[];
      try {
          let rs: any = await this.aipnService.getaipn_statusflgN(info);
          let data:any = rs.results.rows;
          console.log('N',rs);
          if (rs.results.ok){
            this.itemsExeclitemAipnstatusflgN = data;
            this.itemAipnstatusflgN = new MatTableDataSource<_aipnstatusFlgN>(data);
            this.ngAfterViewInit_aipnstatusflgN();
    
          }
        } catch (error: any) {
            console.log();
        }
    
      } 
 //get IDPM
onOptionsSelected(){
  console.log('ward',this.selecteditemIdpm);
  
 }
 exportAsXLSXFlagN():void {
  this.excelService.exportAsExcelFile(this.itemsExeclitemAipnstatusflgN, 'ExporttoExcel');
}

exportAsXLSXFlagA():void {
this.excelService.exportAsExcelFile(this.itemsExeclitemAipnstatusflgA, 'ExporttoExcel');
}

exportAsXLSXFlagC():void {
this.excelService.exportAsExcelFile(this.itemsExeclitemAipnstatusflgC, 'ExporttoExcel');
}
exportAsXLSXFlagD():void {
this.excelService.exportAsExcelFile(this.itemsExeclitemAipnstatusflgD, 'ExporttoExcel');
}

 _setDataSource(indexNumber) {
  setTimeout(() => {
    switch (indexNumber) {
      case 0:
        !this.itemAipnstatusflgA.paginator ? this.itemAipnstatusflgA.paginator = this.paginator : null;
        break;
      case 1:
        !this.itemAipnstatusflgC.paginator ? this.itemAipnstatusflgC.paginator = this.paginator2 : null;
        break;
      case 2:
        !this.itemAipnstatusflgD.paginator ? this.itemAipnstatusflgD.paginator = this.paginator3 : null;
        break;
      case 3:
        !this.itemAipnstatusflgN.paginator ? this.itemAipnstatusflgN.paginator = this.paginator4 : null;
    }
  });
} 
}

