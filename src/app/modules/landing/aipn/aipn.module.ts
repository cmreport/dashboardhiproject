import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { AipnRoutingModule } from './aipn-routing.module';
import { AipnComponent } from './aipn.component';

import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    AipnComponent
  ],
  providers:[ExcelService],
  imports: [
    AipnRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class AipnModule { }
