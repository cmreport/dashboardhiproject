import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamplePageComponentComponent } from './example-page.component.component';

describe('ExamplePageComponentComponent', () => {
  let component: ExamplePageComponentComponent;
  let fixture: ComponentFixture<ExamplePageComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamplePageComponentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExamplePageComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
