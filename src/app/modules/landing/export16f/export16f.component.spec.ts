import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Export16fComponent } from './export16f.component';

describe('Export16fComponent', () => {
  let component: Export16fComponent;
  let fixture: ComponentFixture<Export16fComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Export16fComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Export16fComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
