import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/export16f/export16f.model'

import { Export16fService } from '../../../services/export16f.service'


export interface _ins {
  HN : any;
  INSCL  : any ;
  SUBTYPE  : any ;
  CID  : any ;
  HCODE  : any ;
  DATEEXP  : any ;
  HOSPMAIN  : any ;
  HOSPSUB  : any ;
  GOVCODE  : any ;
  GOVNAME  : any ;
  PERMITNO  : any ;
  DOCNO  : any ;
  OWNRPID  : any ;
  OWNNAME  : any ;
  AN  : any ;
  SEQ  : any ;
  SUBINSCL  : any ;
  RELINSCL  : any ;
  HTYPE  : any ;

  
}

export interface _pat {
  HCODE  : any ;
  HN  : any ;
  CHANGWAT  : any ;
  AMPHUR  : any ;
  DOB  : any ;
  SEX  : any ;
  MARRIAGE  : any ;
  OCCUPA  : any ;
  NATION  : any ;
  PERSON_ID  : any ;
  NAMEPAT  : any ;
  TITLE  : any ;
  FNAME  : any ;
  LNAME  : any ;
  IDTYPE  : any ;

}

export interface _opd{
  HN  : any ;
  CLINIC  : any ;
  DATEOPD  : any ;
  TIMEOPD  : any ;
  SEQ  : any ;
  UUC  : any ;
  DETAIL  : any ;
  BTEMP  : any ;
  SBP  : any ;
  DBP  : any ;
  PR  : any ;
  RR  : any ;
  OPTYPE  : any ;
  TYPEIN  : any ;
  TYPEOUT  : any ;
}

export interface _orf
{
    HN  : any ;
    DATEOPD  : any ;
    CLINIC  : any ;
    REFER  : any ;
    REFERTYPE  : any ;
    SEQ  : any ;
    REFERDATE  : any ;

}

export interface _odx
{
    HN  : any ;
    DATEDX  : any ;
    CLINIC  : any ;
    DIAG  : any ;
    DXTYPE  : any ;
    DRDX  : any ;
    PERSON_ID  : any ;
    SEQ  : any ;

}
export interface _oop
{
    HN  : any ;
    DATEOPD  : any ;
    CLINIC  : any ;
    OPER  : any ;
    DROPID  : any ;
    PERSON_ID  : any ;
    SEQ  : any ;
    SERVPRICE  : any ;

}

export interface _ipd
{
    HN  : any ;
    AN  : any ;
    DATEADM  : any ;
    TIMEADM  : any ;
    DATEDSC  : any ;
    TIMEDSC  : any ;
    DISCHS  : any ;
    DISCHT  : any ;
    WARDDSC  : any ;
    DEPT  : any ;
    ADM_W  : any ;
    UUC  : any ;
    SVCTYPE  : any ;

}

export interface _irf
{
    AN  : any ;
    REFER  : any ;
    REFERTYPE  : any ;

}

export interface _idx
{
    AN  : any ;
    DIAG  : any ;
    DXTYPE  : any ;
    DRDX  : any ;

}

export interface _iop
{
    AN  : any ;
    OPER  : any ;
    OPTYPE  : any ;
    DROPID  : any ;
    DATEIN  : any ;
    TIMEIN  : any ;
    DATEOUT  : any ;
    TIMEOUT  : any ;

}
export interface _cht
{
    HN  : any ;
    AN  : any ;
    DATE  : any ;
    TOTAL  : any ;
    PAID  : any ;
    PTTYPE  : any ;
    PERSON_ID  : any ;
    SEQ  : any ;
    OPD_MEMO  : any ;
    INVOICE_NO  : any ;
    INVOICE_LT  : any ;

}
export interface _cha
{
    HN  : any ;
    AN  : any ;
    DATE  : any ;
    CHRGITEM  : any ;
    AMOUNT  : any ;
    PERSON_ID  : any ;
    SEQ  : any ;

}

export interface _aer
{
    HN  : any ;
    AN  : any ;
    DATEOPD  : any ;
    AUTHAE  : any ;
    AEDATE  : any ;
    AETIME  : any ;
    AETYPE  : any ;
    REFER_NO  : any ;
    REFERMAINI  : any ;
    IREFTYPE  : any ;
    REFMAINO  : any ;
    OREFTYPE  : any ;
    UCAE  : any ;
    EMTYPE  : any ;
    SEQ  : any ;
    AESTATUS  : any ;
    DALERT  : any ;
    TALERT  : any ;

}

export interface _adp
{
    HN  : any ;
    AN  : any ;
    DATEOPD  : any ;
    TYPE  : any ;
    CODE  : any ;
    QTY  : any ;
    RATE  : any ;
    SEQ  : any ;
    CAGCODE  : any ;
    DOSE  : any ;
    CA_TYPE  : any ;
    SERIALNO  : any ;
    TOTCOPAY  : any ;
    USE_STATUS  : any ;
    TOTAL  : any ;
    QTYDAY  : any ;
    TMLTCODE  : any ;
    STATUS1  : any ;
    BI  : any ;
    CLINIC  : any ;
    ITEMSRC  : any ;
    PROVIDER  : any ;
    GRAVIDA  : any ;
    GA_WEEK  : any ;
    DCIP : any;
    LMP  : any ;
    SP_ITEM  : any ;

}

export interface _lvd
{
    SEQLVD  : any ;
    AN  : any ;
    DATEOUT  : any ;
    TIMEOUT  : any ;
    DATEIN  : any ;
    TIMEIN  : any ;
    QTYDAY  : any ;

}

export interface _dru
{
    HCODE  : any ;
    HN  : any ;
    AN  : any ;
    CLINIC  : any ;
    PERSON_ID  : any ;
    DATE_SERV  : any ;
    DID  : any ;
    DIDNAME  : any ;
    AMOUNT  : any ;
    DRUGPRICE  : any ;
    DRUGCOST  : any ;
    DIDSTD  : any ;
    UNIT  : any ;
    UNIT_PACK  : any ;
    SEQ  : any ;
    DRUGREMARK  : any ;
    PA_NO  : any ;
    TOTCOPAY  : any ;
    USE_STATUS  : any ;
    TOTAL  : any ;
    SIGCODE  : any ;
    SIGTEXT  : any ;
    PROVIDER  : any ;

}

export interface _labfu
{
    HCODE  : any ;
    HN  : any ;
    PERSON_ID  : any ;
    DATESERV  : any ;
    SEQ  : any ;
    LABTEST  : any ;
    LABRESULT  : any ;
}


@Component({
  selector: 'app-export16f',
  templateUrl: './export16f.component.html',
  styleUrls: ['./export16f.component.scss']
})
export class Export16fComponent {

  startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  isCheckbox : boolean = false;
  selecteditem_lfund:any='';
  
countRowsins :any;
countRowspat :any;
countRowsopd :any;
countRowsorf :any;
countRowsodx :any;
countRowsoop :any;
countRowsipd :any;
countRowsirf :any;
countRowsidx :any;
countRowsiop :any;
countRowscht :any;
countRowscha :any;
countRowsaer :any;
countRowsadp :any;
countRowslvd :any;
countRowsdru :any;
countRowslabfu :any;

  //l_funds
  itemlfund:any = [];
  info:any = '';

  
  //item 16F
  itemins:any=[];
  itempat:any=[];
  itemaer:any=[];
  itemadp:any=[];
  itemcha:any=[];
  itemcht:any=[];
  itemdru:any=[];
  itemidx:any=[];
  itemiop:any=[];
  itemirf:any=[];
  itemlabfu:any=[];
  itemlvd:any=[];
  itemodx:any=[];
  itemoop:any=[];
  itemorf:any=[];
  itemopd:any=[];
  itemipd:any=[];
  
  //Export File
  execlins:any=[];
  execlpat:any=[];
  execlaer:any=[];
  execladp:any=[];
  execlcha:any=[];
  execlcht:any=[];
  execldru:any=[];
  execlidx:any=[];
  execliop:any=[];
  execlirf:any=[];
  execllabfu:any=[];
  execllvd:any=[];
  execlodx:any=[];
  execloop:any=[];
  execlorf:any=[];
  execlopd:any=[];
  execlipd:any=[];
 
insdisplayedColumns: string[] = ['HN','INSCL','CID','DATEEXP','HCODE', 'HOSPMAIN', 'HOSPSUB','GOVCODE','GOVNAME',
'PERMITNO','DOCNO','OWNRPID','OWNNAME','AN','SEQ','SUBINSCL','RELINSCL','HTYPE'];

patdisplayedColumns: string[] = ['HCODE','HN','CHANGWAT','AMPHUR','DOB', 'SEX', 'MARRIAGE','OCCUPA','NATION',
'PERSON_ID','NAMEPAT','TITLE','FNAME','LNAME','IDTYPE'];

opddisplayedColumns: string[] = ['HN','CLINIC','DATEOPD','TIMEOPD','SEQ', 'UUC', 'DETAIL','BTEMP','SBP',
'DBP','PR','RR','OPTYPE','TYPEIN','TYPEOUT'];

orfdisplayedColumns: string[] = ['HN','DATEOPD','CLINIC','REFER', 'REFERTYPE', 'SEQ','REFERDATE'];
 
odxdisplayedColumns: string[] = ['HN','DATEDX','CLINIC','DIAG', 'DXTYPE', 'DRDX','PERSON_ID','SEQ'];

oopdisplayedColumns: string[] = ['HN','DATEOPD','CLINIC','OPER', 'DROPID', 'PERSON_ID','SEQ','SERVPRICE'];

ipdisplayedColumns: string[] = ['HN','AN','DATEADM','TIMEADM', 'DATEDSC', 'TIMEDSC','DISCHS','DISCHT','WARDDSC',
  'DEPT','ADM_W','UUC','SVCTYPE'];

irfdisplayedColumns: string[] = ['AN','REFER', 'REFERTYPE'];

idxdisplayedColumns: string[] = ['AN','DIAG', 'DXTYPE', 'DRDX'];

iopdisplayedColumns: string[] = ['AN','OPER','OPTYPE', 'DROPID','DATEIN','TIMEIN', 'DATEOUT','TIMEOUT'];

chtdisplayedColumns: string[] = ['HN','AN','DATE', 'TOTAL','PAID','PTTYPE', 'PERSON_ID','SEQ','OPD_MEMO','INVOICE_NO','INVOICE_LT'];

chadisplayedColumns: string[] = ['HN','AN','DATE', 'CHRGITEM','AMOUNT','PERSON_ID', 'SEQ'];

aerdisplayedColumns: string[] = ['HN','AN','DATEOPD','AUTHAE','AEDATE', 'AETIME', 'AETYPE','REFER_NO','REFERMAINI',
'IREFTYPE','REFMAINO','OREFTYPE','UCAE','EMTYPE','SEQ','AESTATUS','DALERT','TALERT'];

adpdisplayedColumns: string[] = ['HN','AN','DATEOPD','TYPE','CODE', 'QTY', 'RATE','SEQ','CAGCODE',
'DOSE','CA_TYPE','SERIALNO','TOTCOPAY','USE_STATUS','TOTAL','QTYDAY','TMLTCODE','STATUS1','BI','CLINIC',
'ITEMSRC','PROVIDER','GRAVIDA','GA_WEEK','DCIP','LMP','SP_ITEM'];

lvddisplayedColumns: string[] = ['SEQLVD','AN','DATEOUT', 'TIMEOUT','DATEIN','TIMEIN', 'QTYDAY'];

drudisplayedColumns: string[] = ['HCODE','HN','AN','CLINIC','PERSON_ID', 'DATE_SERV', 'DID','DIDNAME','AMOUNT',
'DRUGPRICE','DRUGCOST','UNIT','UNIT_PACK','SEQ','DRUGREMARK','PA_NO','TOTCOPAY','USE_STATUS','TOTAL','SIGCODE',
'SIGTEXT','PROVIDER'];

labfudisplayedColumns: string[] = ['HCODE','HN','PERSON_ID', 'DATESERV','SEQ','LABTEST', 'LABRESULT'];

@ViewChild('pins') pins: MatPaginator;
@ViewChild('ppat') ppat: MatPaginator;
@ViewChild('paer') paer: MatPaginator;
@ViewChild('padp') padp: MatPaginator;
@ViewChild('pcha') pcha: MatPaginator;
@ViewChild('pcht') pcht: MatPaginator;
@ViewChild('pdru') pdru: MatPaginator;
@ViewChild('pidx') pidx: MatPaginator;
@ViewChild('piop') piop: MatPaginator;
@ViewChild('pirf') pirf: MatPaginator;
@ViewChild('plabfu') plabfu: MatPaginator;
@ViewChild('plvd') plvd: MatPaginator;
@ViewChild('podx') podx: MatPaginator;
@ViewChild('poop') poop: MatPaginator;
@ViewChild('porf') porf: MatPaginator;
@ViewChild('popd') popd: MatPaginator;
@ViewChild('pipd') pipd: MatPaginator;


ngAfterViewInit_ins() {
  this.itemins.paginator = this.pins;
}
ngAfterViewInit_pat() {
  this.itempat.paginator = this.ppat;
}
ngAfterViewInit_opd() {
  this.itemopd.paginator = this.popd;
}
ngAfterViewInit_orf() {
  this.itemorf.paginator = this.porf;
}
ngAfterViewInit_odx() {
  this.itemodx.paginator = this.podx;
}
ngAfterViewInit_oop() {
  this.itemoop.paginator = this.poop;
}
ngAfterViewInit_ipd() {
  this.itemipd.paginator = this.pipd;
}
ngAfterViewInit_irf() {
  this.itemirf.paginator = this.pirf;
}
ngAfterViewInit_idx() {
  this.itemidx.paginator = this.pidx;
}
ngAfterViewInit_iop() {
  this.itemiop.paginator = this.piop;
}
ngAfterViewInit_cht() {
  this.itemcht.paginator = this.pcht;
}
ngAfterViewInit_cha() {
  this.itemcha.paginator = this.pcha;
}
ngAfterViewInit_aer() {
  this.itemaer.paginator = this.paer;
}
ngAfterViewInit_adp() {
  this.itemadp.paginator = this.padp;
}
ngAfterViewInit_lvd() {
  this.itemlvd.paginator = this.plvd;
}
ngAfterViewInit_dru() {
  this.itemdru.paginator = this.pdru;
}
ngAfterViewInit_labfu() {
  this.itemlabfu.paginator = this.plabfu;
}

  constructor(
    private router: Router,
    private export16fService:Export16fService,
    private excelService : ExcelService
) {} 

ngOnInit(): void {
  this.isCheckbox  = true;
  this.getLoadData()
  //this.getExport()
}

//get DATA TABLE
async getLoadData(){
  await this.getLookup_lfund();
}

//get DATA Table debt_account of db HICM
async getLookup_lfund(){
  let lcfund :any = await this.export16fService.getselect_lfunds();
  console.log('l_funds',lcfund);
  
  this.itemlfund = lcfund;
}


async getExport(){
  let lcCode:any = this.selecteditem_lfund; //OFC
  let info: any = {
  "projcode": lcCode,
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info:',info);

  await this.getIns(info);
  await this.getPat(info);
  await this.getOpd(info);
  await this.getOrf(info);
  await this.getOdx(info);
  await this.getOop(info);
}


async getIns(info){
  
  
  this.itemins= [];
  this.execlins=[];
  try {
    console.log('insrs');
      let rs: any = await this.export16fService.getins(info);
      console.log('rs:',rs);
      
      let data:any = rs.results.rows;
      let xx:string = data
      this.countRowsins = xx.length
      console.log('ins',rs);
      if (rs.results.ok){
        this.execlins = data;
        this.itemins = new MatTableDataSource<_ins>(data);
        this.ngAfterViewInit_ins();

      }
    } catch (error: any) {
        console.log();
    }
}

async getPat(info){
    this.itempat= [];
    this.execlpat=[];
    try {
        let rs: any = await this.export16fService.getpat(info);
        let data:any = rs.results.rows;
        let xx:string = data
        this.countRowspat = xx.length
        console.log('pat',rs);
        if (rs.results.ok){
          this.execlpat = data;
          this.itempat = new MatTableDataSource<_pat>(data);
          this.ngAfterViewInit_pat();
  
        }
      } catch (error: any) {
          console.log();
      }
    }
async getOpd(info){
      this.itemopd= [];
      this.execlopd=[];
      try {
          let rs: any = await this.export16fService.getopd(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowsopd = xx.length
          console.log('opd',rs);
          if (rs.results.ok){
            this.execlopd = data;
            this.itemopd = new MatTableDataSource<_opd>(data);
            this.ngAfterViewInit_opd();
    
          }
        } catch (error: any) {
            console.log();
        }
}
  
async getOrf(info){
        this.itemorf= [];
        this.execlorf=[];
        try {
            let rs: any = await this.export16fService.getorf(info);
            let data:any = rs.results.rows;
            let xx:string = data
            this.countRowsorf = xx.length

            console.log('orf',rs);
            if (rs.results.ok){
              this.execlorf = data;
              this.itemorf = new MatTableDataSource<_orf>(data);
              this.ngAfterViewInit_orf();
      
            }
          } catch (error: any) {
              console.log();
          }
}
    
async getOdx(info){
  this.itemodx= [];
  this.execlodx=[];
  try {
      let rs: any = await this.export16fService.getodx(info);
      let data:any = rs.results.rows;
      let xx:string = data
      this.countRowsodx = xx.length

      console.log('orf',rs);
      if (rs.results.ok){
        this.execlodx = data;
        this.itemodx = new MatTableDataSource<_odx>(data);
        this.ngAfterViewInit_odx();

      }
    } catch (error: any) {
        console.log();
    }
}    

async getOop(info){
  this.itemoop= [];
  this.execloop=[];
  try {
      let rs: any = await this.export16fService.getoop(info);
      let data:any = rs.results.rows;
      let xx:string = data
      this.countRowsoop = xx.length

      console.log('oop',rs);
      if (rs.results.ok){
        this.execloop = data;
        this.itemoop = new MatTableDataSource<_oop>(data);
        this.ngAfterViewInit_oop();

      }
    } catch (error: any) {
        console.log();
    }
}  
onOptionsSelected(){
  this.isCheckbox  = false;
  console.log('check_lfund',this.selecteditem_lfund);
  
}


logCheckbox() {
  if(this.isCheckbox === true) {
    this.selecteditem_lfund = '';
  }
  console.log(this.isCheckbox);
}
_setDataSource(indexNumber) {
  setTimeout(() => {
    switch (indexNumber) {
      case 0:
        !this.itemins.paginator ? this.itemins.paginator = this.pins : null;
        break;
      case 1:
        !this.itempat.paginator ? this.itempat.paginator = this.ppat : null;
        break;
      case 2:
        !this.itemopd.paginator ? this.itemopd.paginator = this.popd : null;
        break;
      case 3:
        !this.itemorf.paginator ? this.itemorf.paginator = this.porf : null;
        break;
      case 4:
        !this.itemodx.paginator ? this.itemodx.paginator = this.podx : null;
        break;
      case 5:
        !this.itemoop.paginator ? this.itemoop.paginator = this.poop : null;
        break;
      case 6:
        !this.itemipd.paginator ? this.itemipd.paginator = this.pipd : null;
        break;
      case 7:
        !this.itemirf.paginator ? this.itemirf.paginator = this.pirf : null;
      case 8:
        !this.itemidx.paginator ? this.itemidx.paginator = this.pidx : null;
        break;
      case 9:
        !this.itemiop.paginator ? this.itemiop.paginator = this.piop : null;
        break;
      case 10:
        !this.itemcht.paginator ? this.itemcht.paginator = this.pcht : null;
      case 11:
        !this.itemcha.paginator ? this.itemcha.paginator = this.pcha : null;
        break;
      case 12:
        !this.itemaer.paginator ? this.itemaer.paginator = this.paer : null;
        break;
      case 13:
        !this.itemadp.paginator ? this.itemadp.paginator = this.padp : null;
        break;
      case 14:
        !this.itemlvd.paginator ? this.itemlvd.paginator = this.plvd : null;
      case 15:
        !this.itemdru.paginator ? this.itemdru.paginator = this.pdru : null;
        break;
      case 16:
        !this.itemlabfu.paginator ? this.itemlabfu.paginator = this.plabfu : null;
    }

  });
}

exportAsXLSXins():void {
  this.excelService.exportAsExcelFile(this.execlins, 'ExporttoExcel');
}
exportAsXLSXpat():void {
  this.excelService.exportAsExcelFile(this.execlpat, 'ExporttoExcel');
}
exportAsXLSXopd():void {
  this.excelService.exportAsExcelFile(this.execlopd, 'ExporttoExcel');
}
exportAsXLSXorf():void {
  this.excelService.exportAsExcelFile(this.execlorf, 'ExporttoExcel');
}
exportAsXLSXodx():void {
  this.excelService.exportAsExcelFile(this.execlodx, 'ExporttoExcel');
}
exportAsXLSXoop():void {
  this.excelService.exportAsExcelFile(this.execloop, 'ExporttoExcel');
}
}
