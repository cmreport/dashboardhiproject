import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IoprtComponent } from './ioprt.component';

const routes: Routes = [
  {
    path:'',component:IoprtComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IoprtRoutingModule { }
