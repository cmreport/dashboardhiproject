import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IoprtComponent } from './ioprt.component';

describe('IoprtComponent', () => {
  let component: IoprtComponent;
  let fixture: ComponentFixture<IoprtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IoprtComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IoprtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
