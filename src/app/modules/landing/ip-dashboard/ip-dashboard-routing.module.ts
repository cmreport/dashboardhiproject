import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IpDashboardComponent} from '../ip-dashboard/ip-dashboard.component'
const routes: Routes = [
  {
    path:'',component:IpDashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IpDashboardRoutingModule { }
