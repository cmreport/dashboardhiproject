import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { IpDashboardComponent } from '../ip-dashboard/ip-dashboard.component';
import { IpDashboardRoutingModule } from './ip-dashboard-routing.module';


@NgModule({
  declarations: [
    IpDashboardComponent
  ],
  imports: [
    IpDashboardRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class IpDashboardModule { }
