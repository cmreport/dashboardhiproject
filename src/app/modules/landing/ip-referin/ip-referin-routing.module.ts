import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IpReferinComponent } from './ip-referin.component';
const routes: Routes = [
  {
    path:'',component:IpReferinComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IpReferinRoutingModule { }
