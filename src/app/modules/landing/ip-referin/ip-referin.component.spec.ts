import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IpReferinComponent } from './ip-referin.component';

describe('IpReferinComponent', () => {
  let component: IpReferinComponent;
  let fixture: ComponentFixture<IpReferinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IpReferinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IpReferinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
