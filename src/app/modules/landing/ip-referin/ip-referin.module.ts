import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';
import { ExcelService } from 'app/services/excel.service';

import { IpReferinRoutingModule } from './ip-referin-routing.module';
import { IpReferinComponent } from './ip-referin.component';


@NgModule({
  declarations: [
    IpReferinComponent
  ],
  providers:[ExcelService],
  imports: [
    IpReferinRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class IpReferinModule { }
