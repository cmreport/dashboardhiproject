import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IpReferoutComponent } from './ip-referout.component';
const routes: Routes = [
  {
    path:'',component:IpReferoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IpReferoutRoutingModule { }
