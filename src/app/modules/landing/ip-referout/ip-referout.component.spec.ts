import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IpReferoutComponent } from './ip-referout.component';

describe('IpReferoutComponent', () => {
  let component: IpReferoutComponent;
  let fixture: ComponentFixture<IpReferoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IpReferoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IpReferoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
