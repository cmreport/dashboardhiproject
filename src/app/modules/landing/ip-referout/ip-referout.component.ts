import { Component,ViewEncapsulation,ViewChild, OnInit } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption,number } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/ip-referout/ip-referout.model'

import { IpReferService } from '../../../services/iprefer-service';

export interface _dx {
  code: any;
  name: any;
  total:any;
}

export interface _ward {
  code: any;
  name: any;
  total:any;
}

export interface _rfrcs {
  code: any;
  name: any;
  total:any;
}

export interface _rfrlct {
  code: any;
  name: any;
  total:any;
}

@Component({
  selector: 'app-ip-referout',
  templateUrl: './ip-referout.component.html',
  styleUrls: ['./ip-referout.component.scss']
})
export class IpReferoutComponent implements OnInit {

  itemdx:any=[];
  itemdxExecl:any=[];

  itemward:any=[];
  itemwardExecl:any=[];
  
  itemrfrcs:any=[];
  itemrfrcsExecl:any=[];

  itemrfrlct:any=[];
  itemrfrlctExecl:any=[];  
   //OP Visit
   Sdata:any;
   Sclinic: any;
   Spttype: any;
   Sgender: any;
   Sage: any;
   Smonth: any;
   maxopvisit_m: any;
   intervalopvisit_m :any;
   //sum incoth
   sincothmm:any;
   sincothinscl:any;
 
   lineChart!: EChartsOption;
   lineBarChart!: EChartsOption;
   
   breadCrumbItems!: Array<{}>;
   pieChart!: EChartsOption;
   scatterChart!: EChartsOption;
   bubbleChart!: EChartsOption;
   customizepieChart!: EChartsOption;
   gaugeChart!: EChartsOption;
 
   ageChart!: EChartsOption;
   genderChart!: EChartsOption;
   pttypeChart!: EChartsOption;
   clinicChart!: EChartsOption;
   monthChart!: EChartsOption;
   incoth_pttypeChart!:EChartsOption;
   incoth_monthChart!:EChartsOption;

  selectedyearbudget:any;
  selectedWard :any;
  nameidpm: any;

  itemdxdisplayedColumns: string[] = ['code','name','total'];
  itemwarddisplayedColumns: string[] = ['code','name','total'];
  itemrfrcsdisplayedColumns: string[] = ['code','name','total'];
  itemrfrlctdisplayedColumns: string[] = ['code','name','total'];

@ViewChild(MatPaginator) paginator: MatPaginator;
ngAfterViewInit_dx() {
    this.itemdx.paginator = this.paginator;
  }
  ngAfterViewInit_ward() {
    this.itemward.paginator = this.paginator;
  }
  ngAfterViewInit_rfrcs() {
    this.itemrfrcs.paginator = this.paginator;
  }
 
  ngAfterViewInit_rfrlct() {
    this.itemrfrlct.paginator = this.paginator;
  }
  constructor(
    private router : Router,
    private ipReferService : IpReferService,
    private alertService : AlertService,
    private excelService : ExcelService
){}
 
async  ngOnInit() {
  this.selectedyearbudget = sessionStorage.getItem('yearbudget');
  this.selectedWard = sessionStorage.getItem('ward');
  console.log('D',this.selectedWard);
  await this.getData()
}

async getData(){
   // console.log("กรุณเลือกปีงบประมาณ");
   let info: any = {"yearbudget":this.selectedyearbudget,
   "ward":this.selectedWard}
 console.log('info:',info);
 await this.getSummary(info);
 await this.getnameward(info);
 await this.tab_ward(info);
 await this.tab_dx(info);
 await this.tab_rfrcs(info);
 await this.tab_rfrlct(info);
 await this._prepareChart_Refer();
 await this._prepareChartIncoth_Refer();

}

async getSummary(info:any) {
  let Sdata: any;
  try {
      let rs: any = await this.ipReferService.getReferoutSummary(info);
      // console.log(rs);
      if (rs.ok) {
          Sdata = rs;
          this.Sclinic = rs.clinic;
          this.Spttype = rs.pttype;
          this.Sage = rs.age;
          this.Sgender = rs.gender;
          this.Smonth = rs.month;
          this.sincothmm = rs.sincothmm;
          this.sincothinscl = rs.sincothinscl;
          this.maxopvisit_m = rs.maxopvisit_m;
          
      } else {
          Sdata = "ไม่พบข้อมูล";
      }
  } catch (error) {
      console.log(error);
      Sdata = "ไม่พบข้อมูล";
  }
  this.Sdata = Sdata;
}

async getnameward(info){
// console.log('getInfo'); 
this.nameidpm = '';
// console.log('namecln',info);

try {
    let rs:any = await this.ipReferService.getnameward(info);
    // console.log('rs:',rs);     
    let data:any = rs[0];
      if (rs.length > 0){
          this.nameidpm= data.nameidpm;
      }
  } catch(error:any){
      console.log();
  }
}

async tab_dx(info){
  this.itemdx= [];
  this.itemdxExecl=[];
  try {
      let rs: any = await this.ipReferService.getDx20Referout(info);
      let data:any = rs.results.rows;
      // console.log(rs);
      if (rs.results.ok){
        this.itemdxExecl = data;
        this.itemdx = new MatTableDataSource<_dx>(data);
        this.ngAfterViewInit_dx();

      }
    } catch (error: any) {
        console.log();
    }

  }

  async tab_ward(info){
    this.itemward= [];
    this.itemwardExecl=[];
    try {
        let rs: any = await this.ipReferService.getCLN20Referout(info);
        let data:any = rs.results.rows;
        // console.log(rs);
        if (rs.results.ok){
          this.itemwardExecl = data;
          this.itemward = new MatTableDataSource<_ward>(data);
          this.ngAfterViewInit_ward();
  
        }
      } catch (error: any) {
          console.log();
      }
  
    }
  

    async tab_rfrcs(info){
      this.itemrfrcs= [];
      this.itemrfrcsExecl=[];
      try {
          let rs: any = await this.ipReferService.getRfrcs20Referout(info);
          let data:any = rs.results.rows;
          // console.log(rs);
          if (rs.results.ok){
            this.itemrfrcsExecl = data;
            this.itemrfrcs = new MatTableDataSource<_rfrcs>(data);
            this.ngAfterViewInit_rfrcs();
    
          }
        } catch (error: any) {
            console.log();
        }
    
      }
    
      async tab_rfrlct(info){
        this.itemrfrlct= [];
        this.itemrfrlctExecl=[];
        try {
            let rs: any = await this.ipReferService.getHcode20Referout(info);
            let data:any = rs.results.rows;
            // console.log(rs);
            if (rs.results.ok){
              this.itemrfrlctExecl = data;
              this.itemrfrlct = new MatTableDataSource<_rfrlct>(data);
              this.ngAfterViewInit_rfrlct();
      
            }
          } catch (error: any) {
              console.log();
          }
      
        }
      


async _prepareChart_Refer() {
let catagory: any = [];
let data: any =[]; 
let ymax :any;
let yinterval:any;

const monthName = [
    "",
    "ม.ค.",
    "ก.พ.",
    "มี.ค.",
    "เม.ย.",
    "พ.ค.",
    "มิ.ย.",
    "ก.ค.",
    "ส.ค.",
    "ก.ย.",
    "ต.ค.",
    "พ.ย.",
    "ธ.ค.",
]

for(let item of this.Smonth){
    catagory.push(monthName[item.name]);
    data.push(item.value);
};
for (let i of this.maxopvisit_m){
      ymax = i.value;
      yinterval =i.strinter;
}
this.monthChart = {
  
    grid: {
        zlevel: 0,
        borderWidth: 0,
        backgroundColor: "rgba(0,0,0,0)",
        borderColor: "rgba(0,0,0,0)",
    },
    tooltip: {
        trigger: "axis",
        axisPointer: { type: "cross", crossStyle: { color: "#999" } },
    },
    toolbox: {
        left: 0,
        top: 0,
    },
    color: ["#2ab57d", "#5156be", "#fd625e"],
    legend: {
        data: ["Visit"],
        textStyle: { color: "#858d98" },
    },
    xAxis: [
        {
            type: "category",
            data: catagory,
            axisPointer: { type: "shadow" },
            axisLine: { lineStyle: { color: "#858d98" } },
        },
    ],
    yAxis: [
        {
            type: "value",
            name: "ครั้ง",
            min: 0,
            max: ymax, 
          //   interval:  yinterval,
            axisLine: { lineStyle: { color: "#858d98" } },
            splitLine: { lineStyle: { color: "rgba(133, 141, 152, 0.1)" } },
            axisLabel: { formatter: "{value}" },
        },
    ],
    series: [
        {
            name: "Visit",
            type: "bar",
            data: data,
        },
    ],
}

let genderLabel: any = [];
for(let item of this.Sgender){
    let temp = {
        name: item.name,
        icon: 'circle'
    }
    genderLabel.push(temp);
}
this.genderChart = {
    tooltip: { trigger: "item", formatter: "{a} <br/>{b}: {c} ({d}%)" },
    legend: {
        orient: "vertical",
        left: "left",
        data: genderLabel,
        textStyle: { color: "#858d98" },
    },
    color: ["#576CBC", "#F266AB"],
    series: [
        {
            name: 'Gender',
            type: 'pie',
            radius: ['40%', '60%'],
            avoidLabelOverlap: false,
            labelLine: {
                show: false
            },
            data: this.Sgender
        }
    ]
}

let pttypeLabel: any = [];
for(let item of this.Spttype){
    let temp = {
        name: item.name,
        icon: 'circle'
    }
    pttypeLabel.push(temp);
}

this.pttypeChart = {
    tooltip: { trigger: "item", formatter: "{a} <br/>{b}: {c} ({d}%)" },
    legend: {
        orient: "horizontal",
        left: "left",
        data: pttypeLabel,
        textStyle: { color: "#858d98" },
    },
    color: ["#98D8AA", "#F3E99F", "#F7D060", "#FF6D60", "#B01E68", "#576CBC", "#001253", "#F266AB"],
    series: [
        {
            name: 'สิทธิ์การรักษา',
            type: 'pie',
            radius: ['40%', '60%'],
            avoidLabelOverlap: false,
            labelLine: {
                show: false
            },
            data: this.Spttype
        }
    ]
}

let male = [];
let female = [];
let label = [];
for(let item of this.Sage){
    male.push(item.male);
    female.push(item.female*-1);
    label.push(item.name);
}

this.ageChart = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    legend: {
        data: [
            {
                "name":'Male',
                "icon":'circle'
            },
            {
                "name":'Female',
                "icon":'circle'
            }
            ]
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: [
        {
            type: 'value'
        }
    ],
    yAxis: [
        {
            type: 'category',
            axisTick: {
                show: false
            },
            data: label
        }
    ],
    series: [
        {
            name: 'Male',
            type: 'bar',
            stack: 'Total',
            label: {
                show: false,
                position: 'right',
            },
            emphasis: {
                focus: 'series'
            },
            data: male
        },
        {
            name: 'Female',
            type: 'bar',
            stack: 'Total',
            label: {
                show: false,
                position: 'left',

            },
            itemStyle: {
                color: '#F266AB'
            },
            emphasis: {
                focus: 'series'
            },
            data: female
        }
    ]
}
}
async _prepareChartIncoth_Refer() {
let catagory2: any = [];
let data1: any = [];
let data2: any = [];
let max1 = 0;
let max2 = 0.00;
let range1 = 5;
let range2 = 5;

const monthName = [
  "",
  "ม.ค.",
  "ก.พ.",
  "มี.ค.",
  "เม.ย.",
  "พ.ค.",
  "มิ.ย.",
  "ก.ค.",
  "ส.ค.",
  "ก.ย.",
  "ต.ค.",
  "พ.ย.",
  "ธ.ค.",
]


for (let item of this.sincothmm) {

  let total:number = item.total*1;
  if (item.value > max1) {
      max1 = item.value;
  }
  if (total > max2) {
      max2 = total;
  }
  let _data = {
      name: item.name,
      value: item.value,
  }
  let _data2 = {
      name: item.name,
      value: total,
  }
  catagory2.push(item.name);
  data1.push(item.value);
  data2.push(item.total);
};

if (max1 < 10) {
  max1 = 10;
  range1 = 2;
} else if (max1 < 100) {
  max1 = 100;
  range1 = 20;
} else if (max1 < 250) {
  max1 = 250;
  range1 = 50;
} else if (max1 < 500) {
  max1 = 500;
  range1 = 100;
} else if (max1 < 1000) {
  max1 = 1000;
  range1 = 200;
} else if (max1 < 2000) {
  max1 = 2000;
  range1 = 400;
} else if (max1 < 5000) {
  max1 = 5000;
  range1 = 1000;
} else if (max1 < 10000) {
  max1= 10000;
  range1 = 2000;
} else if (max1 < 20000) {
  max1 = 20000;
  range1 = 4000;
} else if (max1 < 50000) {
  max1 = 50000;
  range1 = 10000;
} else {
  range1 = 20000;
}

if (max2 < 10000) {
  max2 = 10000;
  range2 = 2000;
} else if (max2 < 100000) {
  max2 = 100000;
  range2 = 20000;
} else if (max2 < 250000) {
  max2 = 250000;
  range2 = 50000;
} else if (max2 < 500000) {
  max2 = 500000;
  range2 = 100000;
} else if (max2 < 1000000) {
  max2 = 1000000;
  range2 = 200000;
} else if (max2 < 1250000) {
  max2 = 1250000;
  range2 = 250000;
} else if (max2 < 1500000) {
  max2 = 1500000;
  range2 = 300000;
} else if (max2 < 2000000) {
  max2 = 2000000;
  range2 = 400000;
} else if (max2 < 2500000) {
  max2 = 2500000;
  range2 = 500000;
} else if (max2 < 3000000) {
  max2 = 3000000;
  range2 = 600000;
} else {
  max2 = 50000000;
  range2 = 10000000;
}

this.incoth_monthChart = {
  grid: {
      zlevel: 0,
      borderWidth: 0,
      backgroundColor: "rgba(0,0,0,0)",
      borderColor: "rgba(0,0,0,0)",
  },
  tooltip: {
      trigger: "axis",
      axisPointer: { type: "cross", crossStyle: { color: "#999" } },
  },
  toolbox: {
      left: 0,
      top: 0,
  },
  color: ["#2ab57d", "#5156be", "#fd625e"],
  legend: {
      data: ["Visit","Total"],
      textStyle: { color: "#858d98" },
  },
  xAxis: [
      {
          type: "category",
          data: catagory2,
          axisPointer: { type: "shadow" },
          axisLine: { lineStyle: { color: "#858d98" } },
      },
  ],
  yAxis: [
      {
          type: "value",
          name: "Visit",
          min: 0,
          max: max1,
          interval: range1,
          axisLine: { lineStyle: { color: "#858d98" } },
          splitLine: { lineStyle: { color: "rgba(133, 141, 152, 0.1)" } },
          axisLabel: { formatter: "{value}" },
      },
      {
          type: "value",
          name: "Total",
          position: "right",
          min: 0,
          max: max2,
          interval: range2,
          axisLine: { lineStyle: { color: "#858d98" } },
          splitLine: { lineStyle: { color: "rgba(133, 141, 152, 0.1)" } },
          axisLabel: { formatter: "{value} บาท" },
      },
  ],
  series: [
      {
          name: "Visit",
          type: "bar",
          data: data1,
      },
      {
          name: "Total",
          type: "bar",
          yAxisIndex: 1,
          data: data2,
      },
  ],
}

// prepare chart visit by pttype
let catagory3: any = [];
let data3: any = [];
let data4: any = [];
let max3 = 0;
let max4 = 0.00;
let range3 = 5;
let range4 = 5;

for (let item of this.sincothinscl) {

  let total:number = item.total*1;
  if (item.value > max3) {
      max3 = item.value;
  }
  if (total > max4) {
      max4 = total;
  }
  let _data = {
      name: item.name,
      value: item.value,
  }
  let _data2 = {
      name: item.name,
      value: total,
  }
  catagory3.push(item.name);
  data3.push(item.value);
  data4.push(item.total);
};

if (max3 < 10) {
  max3 = 10;
  range3 = 2;
} else if (max3 < 100) {
  max3 = 100;
  range3 = 20;
} else if (max3 < 250) {
  max3 = 250;
  range3 = 50;
} else if (max3 < 500) {
  max3 = 500;
  range3 = 100;
} else if (max3 < 1000) {
  max3 = 1000;
  range3 = 200;
} else if (max3 < 2000) {
  max3 = 2000;
  range3 = 400;
} else if (max3 < 5000) {
  max3 = 5000;
  range3 = 1000;
} else if (max3 < 10000) {
  max3 = 10000;
  range3 = 2000;
} else if (max3 < 20000) {
  max3 = 20000;
  range3 = 4000;
} else if (max3 < 50000) {
  max3 = 50000;
  range3 = 10000;
} else {
  range3 = 20000;
}

if (max4 < 100000) {
  max4 = 100000;
  range4 = 20000;
} else if (max4 < 200000) {
  max4 = 200000;
  range4 = 50000;
} else if (max4 < 500000) {
  max4 = 500000;
  range4 = 100000;
} else if (max4 < 1000000) {
  max4 = 1000000;
  range4 = 500000;
} else if (max4 < 3000000) {
  max4 = 3000000;
  range4 = 600000;
} else if (max4 < 5000000) {
  max4 = 5000000;
  range4 = 1000000;
} else if (max4 < 10000000) {
  max4 = 10000000;
  range4 = 1000000;
} else if (max4 < 25000000) {
  max4 = 25000000;
  range4 = 15000000;
} else if (max4 < 50000000) {
  max4 = 50000000;
  range4 = 10000000;
} else if (max4 < 100000000) {
  max4 = 100000000;
  range4 = 50000000;
} else {
  max4 = 500000000;
  range4 = 100000000;
}

this.incoth_pttypeChart = {
  grid: {
      zlevel: 0,
      borderWidth: 0,
      backgroundColor: "rgba(0,0,0,0)",
      borderColor: "rgba(0,0,0,0)",
  },
  tooltip: {
      trigger: "axis",
      axisPointer: { type: "cross", crossStyle: { color: "#999" } },
  },
  toolbox: {
      left: 0,
      top: 0,
  },
  color: ["#2ab57d", "#5156be", "#fd625e"],
  legend: {
      data: ["Visit","Total"],
      textStyle: { color: "#858d98" },
  },
  xAxis: [
      {
          type: "category",
          data: catagory3,
          axisPointer: { type: "shadow" },
          axisLine: { lineStyle: { color: "#858d98" } },
      },
  ],
  yAxis: [
      {
          type: "value",
          name: "Visit",
          min: 0,
          max: max3,
          interval: range3,
          axisLine: { lineStyle: { color: "#858d98" } },
          splitLine: { lineStyle: { color: "rgba(133, 141, 152, 0.1)" } },
          axisLabel: { formatter: "{value}" },
      },
      {
          type: "value",
          name: "Total",
          position: "right",
          min: 0,
          max: max4,
          interval: range4,
          axisLine: { lineStyle: { color: "#858d98" } },
          splitLine: { lineStyle: { color: "rgba(133, 141, 152, 0.1)" } },
          axisLabel: { formatter: "{value} บาท" },
      },
  ],
  series: [
      {
          name: "Visit",
          type: "bar",
          data: data3,
      },
      {
          name: "Total",
          type: "bar",
          yAxisIndex: 1,
          data: data4,
      },
  ],
}
} 

exportAsXLSXdx():void {
  this.excelService.exportAsExcelFile(this.itemdx, 'ExporttoExcel');
}
exportAsXLSXward():void {
  this.excelService.exportAsExcelFile(this.itemward, 'ExporttoExcel');
}
exportAsXLSXrfrcs():void {
  this.excelService.exportAsExcelFile(this.itemrfrcs, 'ExporttoExcel');
}
exportAsXLSXrfrlct():void {
  this.excelService.exportAsExcelFile(this.itemrfrlct, 'ExporttoExcel');
}
}