import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OpReferinComponent } from './op-referin.component';

const routes: Routes = [
  {
    path:'',component:OpReferinComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OpReferinRoutingModule { }
