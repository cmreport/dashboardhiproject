import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import { OpReferinRoutingModule } from './op-referin-routing.module';
import { OpReferinComponent } from './op-referin.component';

import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';
import { ExcelService } from 'app/services/excel.service';
@NgModule({
  declarations: [
    OpReferinComponent
  ],
  providers:[ExcelService],
  imports: [
    OpReferinRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class OpReferinModule { }
