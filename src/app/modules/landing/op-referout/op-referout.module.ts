import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { OpReferoutRoutingModule } from './op-referout-routing.module';
import { OpReferoutComponent } from './op-referout.component';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';
import { ExcelService } from 'app/services/excel.service';
@NgModule({
  declarations: [
    OpReferoutComponent
  ],
  providers:[ExcelService],
  imports: [
    OpReferoutRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class OpReferoutModule { }
