import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OprtComponent } from './oprt.component';

const routes: Routes = [
  {
    path:'',component:OprtComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OprtRoutingModule { }
