import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';
import { ExcelService } from 'app/services/excel.service';

import { OprtRoutingModule } from './oprt-routing.module';
import { OprtComponent } from './oprt.component';


@NgModule({
  declarations: [
    OprtComponent
  ],
  imports: [
    OprtRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class OprtModule { }
