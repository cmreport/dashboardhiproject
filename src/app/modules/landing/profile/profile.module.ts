import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ProfileComponent } from 'app/modules/landing/profile/profile.component';
import { profileRoutes } from 'app/modules/landing/profile/profile.routing';
import { MatPaginatorModule } from '@angular/material/paginator';
import {FormGroup, FormControl} from '@angular/forms';
import {Component} from '@angular/core';


@NgModule({
  declarations: [
    ProfileComponent
  ],
  imports: [
    RouterModule.forChild(profileRoutes),
    MatPaginatorModule,
    SharedModule
  ]
})
export class ProfileModule { }
