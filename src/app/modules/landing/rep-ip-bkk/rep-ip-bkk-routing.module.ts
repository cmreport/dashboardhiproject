import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepIpBkkComponent } from './rep-ip-bkk.component'
const routes: Routes = [
  {
    path:'',component:RepIpBkkComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepIpBkkRoutingModule { }
