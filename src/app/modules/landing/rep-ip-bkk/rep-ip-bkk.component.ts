import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/rep-ip-bkk/rep-ip-bkk.model'

import { IpbkkService } from '../../../services/ipbkk.service'

export interface _ipbkknull {
  an: any;
  hn: any;
  cid: any;
  fullname: any;
  acc_name:any;
  admitdate: any;
  dchdate: any;
  l_stay: any;
  charge: any;
  paid: any;
  debt: any;
  debt2: any;
  repno: any;
  error_code: any;
  error_name: any;
  remark_data: any;
  total_paid: any;
  rep_diff: any;
  rep_diff2 : any;
  rest_debt:any;
  projcode: any;
}
export interface _ipbkknotnull_stm {

  an: any;
  hn: any;
  cid: any;
  fullname: any;
  acc_name:any;
  admitdate: any;
  dchdate: any;
  l_stay: any;
  charge: any;
  paid: any;
  debt: any;
  debt2: any;
  repno: any;
  error_code: any;
  error_name: any;
  remark_data: any;
  total_paid: any;
  rep_diff: any;
  rep_diff2 : any;
  rest_debt:any;
  projcode: any;
  
  }

export interface _ipbkknotnull {
  hn : any;
  an: any;
  cid: any;
  fullname: any;
  acc_name: any;
  admitdate: any;
  dchdate: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  adjrw: any;
  total_summary: any;
  projcode: any;
  rep_diff: any;
  rep_diff2 : any;
  receipt_no:any;
}

export interface _ipbkkaccbydate {
  dchdate: any;
  allcase: any;
  debit: any;
  nullcase: any;
  nulldebit: any;
  notnullcase: any;
  notnulldebit: any;
  recieve: any;
  diff: any;
}

export interface _errorcode{
  error_code:any;
  error_name:any;
  counterrorcode:any;
}

@Component({
  selector: 'app-rep-ip-bkk',
  templateUrl: './rep-ip-bkk.component.html',
  styleUrls: ['./rep-ip-bkk.component.scss']
})
export class RepIpBkkComponent {
  //count datatable
countRowstab1 :any;
countRowstab2 :any;
countRowstab3 :any;
countRowstab4 :any;
countRowstab5 :any;

//สร้างตัวแปร items,datestart,dateend
//itemerrorcode
itemerrorcode:any=[];
itemerrorcodeExecl:any=[];

//ipbkkaccbydate
itemIpbkkaccbydate : any =[];
itemsExeclIpbkkaccbydate:any = [];

//ipbkknotNull
itemIpbkknotNull : any = [];
itemsExeclIpbkknotNull:any = [];

//ipbkkNull
itemIpbkkNull : any = [];
itemsExeclIpbkkNull:any = [];

//accnull
ipbkkaccnull_count : any=0;
ipbkkaccnull_sum : any=0;

 //debt_account
 itemaccount:any = [];
 selecteditemaccount:any='';

 //รอดำเนินการมี repno แต่ไม่มี stm
ipbkkaccnotnullstm_count : any=0;
ipbkkaccnotnullstm_sum : any=0;

 //ipnotNull_stm
 itemIpbkknotnull_stm : any = [];
 itemsExeclIpbkkNotNull_stm:any = [];

//accnotnull
all_notnullcase: any=0;
debit_notnull: any=0;
recieve: any=0;
sum_diff: any=0;
diffgain: any=0;
diffloss: any=0;
ipbkkcall : any =0;
ipbkksumdebit : any =0;

//circular
percent_null:any = 0;
percent_rep:any = 0;
percent_success:any = 0;

startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
isCheckbox : boolean = false;
// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
ipbkknulldisplayedColumns: string[] = ['hn','an','cid','fullname',
'acc_name', 'admitdate', 'dchdate','charge','paid','debt','repno','projcode','error_code','error_name'];
ipbkknotnullstmdisplayedColumns: string[] = ['hn','an','cid','fullname',
  'acc_name', 'admitdate', 'dchdate','charge','paid','debt','repno','projcode','error_code','error_name'];
ipbkknotnulldisplayedColumns: string[] = ['hn','an','cid','fullname',
'acc_name', 'admitdate', 'dchdate','charge','paid','debt','repno','projcode','adjrw',
'total_summary','rep_diff','rep_diff2','receipt_no'];

ipbkkaccbydatedisplayedColumns: string[] = ['dchdate','allcase','debit','nullcase',
'nulldebit', 'notnullcase', 'notnulldebit','recieve','diff'];

errorcodedisplayedColumns: string[] = ['error_code','error_name','counterror'];

@ViewChild('paginator') paginator: MatPaginator;
@ViewChild('paginator2') paginator2: MatPaginator;
@ViewChild('paginator3') paginator3: MatPaginator;
@ViewChild('paginator4') paginator4: MatPaginator;
@ViewChild('paginator5') paginator5: MatPaginator;

ngAfterViewInit_ipbkknull() {
    this.itemIpbkkNull.paginator = this.paginator;
  }
  ngAfterViewInit_ipbkkNotnull_stm() {
    this.itemIpbkknotnull_stm.paginator = this.paginator2;
  }
  ngAfterViewInit_ipbkknotnull() {
    this.itemIpbkknotNull.paginator = this.paginator3;
  }
  ngAfterViewInit_ipbkkaccbydate() {
    this.itemIpbkkaccbydate.paginator = this.paginator4;
  }
  ngAfterViewInit_ipbkkerrorcode() {
    this.itemerrorcode.paginator = this.paginator5;
  }

  constructor(
    private router: Router,
    private ipbkkService:IpbkkService,
    private excelService : ExcelService
) {} 


ngOnInit(): void {
  this.isCheckbox  = true;
  this.getLoadData()
  //this.getipbkkacc()
}

//get DATA TABLE
async getLoadData(){
    await this.getLookup_debtaccount();
}
//get DATA Table debt_account of db HICM
async getLookup_debtaccount(){
  let lcACC :any = await this.ipbkkService.getselect_debt_account_hi();
  console.log('lcACC',lcACC);
  
  this.itemaccount = lcACC;
}

async getipbkkacc(){

  let lcCode:any = this.selecteditemaccount; //bkk
  let info: any = {
  "accType": lcCode,
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info:',info);
  //ผู้ป่วยส่งเบิกทั้งหมด OK
  await this.ipCounbkkAll(info)
  //ดำเนินการเสร็จ OK
  await this.ipbkkaccnotnull(info);
  //มี repno แต่ไม่มี statement
  await this.ipbkknotnull_stm(info);
  //รอดำเนินการ OK
  await this.ipbkkaccnull(info);
  //tab 1 ok
  await this.tab_ipbkknull(info);
  //tab 2 ok
  await this.tab_ipbkkaccnotnull_stm(info);
  //tab 3 ok
  await this.tab_ipbkknotnull(info);
  //tab 4 ok
  await this.tab_ipbkkaccbydate(info);
  //tab 5 ok
  await this.tab_ipbkkbyerror(info);
  //Sum Total
  await this.ipbkkall();
  //Percents
  await this.circularprogress();
}
async ipbkkall(){
  // this.ipofccall = +this.all_notnullcase + +this.ipofcaccnull_count    ipofcaccnull_sum ipofcaccnotnullstm_sum debit_notnull;
  this.ipbkksumdebit = +this.debit_notnull + +this.ipbkkaccnull_sum + +this.ipbkkaccnotnullstm_sum;
  //console.log('total',this.ipofcsumdebit);
  
}
async tab_ipbkkaccnotnull_stm(info){
  this.itemIpbkknotnull_stm= [];
  this.itemsExeclIpbkkNotNull_stm=[];
  try {
      let rs: any = await this.ipbkkService.getipbkkaccnotnull_stm(info);
      let data:any = rs.results.rows
      let xx:string = data
      console.log('xxxxx',xx);
      
      this.countRowstab5 = xx.length
      
      if (rs.results.ok){
        this.itemsExeclIpbkkNotNull_stm = data;
        this.itemIpbkknotnull_stm = new MatTableDataSource<_ipbkknotnull_stm>(data);
        this.ngAfterViewInit_ipbkkNotnull_stm();

      }
    } catch (error: any) {
        console.log();
    }

  }

async ipbkknotnull_stm(info){
  try {
    let rs: any = await this.ipbkkService.getipbkknotnull_stm(info);
    console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.ipbkkaccnotnullstm_count = rs.results.rows[0].count_an;
      this.ipbkkaccnotnullstm_sum = rs.results.rows[0].sum_debt;
      console.log(this.ipbkkaccnotnullstm_sum);

    }
  } catch (error: any) {
      console.log();

  }

}
async ipCounbkkAll(info){ 
  try {
    let rs: any = await this.ipbkkService.getstmipCount(info);
    let data:any = rs.results;
    console.log('xx',rs);
    if (rs.results.ok){
      this.ipbkkcall = rs.results.rows[0].all_ip ;

    }
  } catch (error: any) {
      console.log(error);
  }
}


async ipbkkaccnull(info){
  try {
    let rs: any = await this.ipbkkService.getipbkkaccnull(info);
    console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.ipbkkaccnull_count = rs.results.rows[0].count_an;
      this.ipbkkaccnull_sum = rs.results.rows[0].sum_debt;
      console.log(this.ipbkkaccnull_count);

    }
  } catch (error: any) {
      console.log();

  }

}
async ipbkkaccnotnull(info){
  try {
    let rs: any = await this.ipbkkService.getipbkkaccnotnull(info);
    console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.all_notnullcase = rs.results.rows[0].all_notnullcase;
      this.debit_notnull = rs.results.rows[0].debit_notnull;
      this.recieve = rs.results.rows[0].recieve;
      this.sum_diff = rs.results.rows[0].sum_diff;
      this.diffgain = rs.results.rows[0].diffgain;
      this.diffloss = rs.results.rows[0].diffloss;

    }
  } catch (error: any) {
      console.log();
  }

}


async tab_ipbkknull(info){
  this.itemIpbkkNull= [];
  this.itemsExeclIpbkkNull=[];
  try {
      let rs: any = await this.ipbkkService.getipbkknull(info);
      let data:any = rs.results.rows;
      let xx:string = data
      this.countRowstab1 = xx.length
      // console.log(rs);
      if (rs.results.ok){
        this.itemsExeclIpbkkNull = data;
        this.itemIpbkkNull = new MatTableDataSource<_ipbkknull>(data);
        this.ngAfterViewInit_ipbkknull();

      }
    } catch (error: any) {
        console.log();
    }

  }

  async tab_ipbkknotnull(info){
      this.itemIpbkknotNull= [];
      this.itemsExeclIpbkknotNull=[];
      try {
          let rs: any = await this.ipbkkService.getipbkknotnull(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab2 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclIpbkknotNull = data;
            this.itemIpbkknotNull = new MatTableDataSource<_ipbkknotnull>(data);
            this.ngAfterViewInit_ipbkknotnull();

          }
        } catch (error: any) {
            console.log();
        }
  }

  async tab_ipbkkaccbydate(info){
      this.itemIpbkkaccbydate= [];
      this.itemsExeclIpbkkaccbydate=[];
      try {
          let rs: any = await this.ipbkkService.getipbkkaccbydate(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab3 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclIpbkkaccbydate = data;
            this.itemIpbkkaccbydate = new MatTableDataSource<_ipbkkaccbydate>(data);
            this.ngAfterViewInit_ipbkkaccbydate();

          }
        } catch (error: any) {
            console.log();
        }

  }
  async tab_ipbkkbyerror(info){
    this.itemerrorcode= [];
    this.itemerrorcodeExecl=[];
    try {
        let rs: any = await this.ipbkkService.gettoperrorcode(info);
        let data:any = rs.results.rows;
        let xx:string = data
        this.countRowstab4 = xx.length
        // console.log(rs);
        if (rs.results.ok){
          this.itemerrorcodeExecl = data;
          this.itemerrorcode = new MatTableDataSource<_errorcode>(data);
          this.ngAfterViewInit_ipbkkerrorcode();

        }
      } catch (error: any) {
          console.log();
      }

}
      exportAsXLSXNull():void {
          this.excelService.exportAsExcelFile(this.itemsExeclIpbkkNull, 'ExporttoExcel');
        }
        exportAsXLSXNotNull_stm():void {
          this.excelService.exportAsExcelFile(this.itemsExeclIpbkkNotNull_stm, 'ExporttoExcel');
        }
      exportAsXLSXnotNull():void {
       this.excelService.exportAsExcelFile(this.itemsExeclIpbkknotNull, 'ExporttoExcel');
     }

     exportAsXLSXAccbyDate():void {
      this.excelService.exportAsExcelFile(this.itemsExeclIpbkkaccbydate, 'ExporttoExcel');
    }
    exportAsXLSXErrorcode():void {
      this.excelService.exportAsExcelFile(this.itemerrorcode, 'ExporttoExcel');
    }
    //get debt_account
    onOptionsSelected(){
      this.isCheckbox  = false;
      console.log('ACC',this.selecteditemaccount);
      
    }
    logCheckbox() {
      if(this.isCheckbox === true) {
        this.selecteditemaccount = '';
      }
      console.log(this.isCheckbox);
    }

    _setDataSource(indexNumber) {
      setTimeout(() => {
        switch (indexNumber) {
          case 0:
            !this.itemIpbkkNull.paginator ? this.itemIpbkkNull.paginator = this.paginator : null;
            break;
          case 1:
              !this.itemsExeclIpbkkNotNull_stm.paginator ? this.itemsExeclIpbkkNotNull_stm.paginator = this.paginator2 : null;
              break;
          case 2:
            !this.itemIpbkknotNull.paginator ? this.itemIpbkknotNull.paginator = this.paginator3 : null;
            break;
          case 3:
            !this.itemIpbkkaccbydate.paginator ? this.itemIpbkkaccbydate.paginator = this.paginator4 : null;
            break;
          case 4:
            !this.itemerrorcode.paginator ? this.itemerrorcode.paginator = this.paginator5 : null
          
          }
      });
    } 

    circularprogress(){
      this.percent_null =  Math.round(this.ipbkkaccnull_count * 100 / this.ipbkkcall)
      this.percent_rep = Math.round(this.ipbkkaccnotnullstm_count * 100 / this.ipbkkcall)
      this.percent_success = Math.round(this.all_notnullcase * 100 / this.ipbkkcall)
      console.log('rep_percent',this.percent_null);
      
    }
}
