import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepIpLgoComponent } from './rep-ip-lgo.component';

describe('RepIpLgoComponent', () => {
  let component: RepIpLgoComponent;
  let fixture: ComponentFixture<RepIpLgoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepIpLgoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepIpLgoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
