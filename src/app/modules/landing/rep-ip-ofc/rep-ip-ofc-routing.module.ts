import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepIpOfcComponent } from './rep-ip-ofc.component'
const routes: Routes = [
  {
    path:'',component:RepIpOfcComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepIpOfcRoutingModule { }
