import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { IpofcService } from '../../../services/ipofc.service'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';

import { ChartType } from '../../landing/rep-ip-ofc/rep-ip-ofc.model'



export interface _ipofcnull {

an: any;
hn: any;
cid: any;
fullname: any;
acc_name:any;
admitdate: any;
dchdate: any;
l_stay: any;
charge: any;
paid: any;
debt: any;
debt2: any;
repno: any;
error_code: any;
error_name: any;
remark_data: any;
total_paid: any;
rep_diff: any;
rep_diff2 : any;
rest_debt:any;
projcode: any;

}

export interface _ipofcnotnull_stm {

  an: any;
  hn: any;
  cid: any;
  fullname: any;
  acc_name:any;
  admitdate: any;
  dchdate: any;
  l_stay: any;
  charge: any;
  paid: any;
  debt: any;
  debt2: any;
  repno: any;
  error_code: any;
  error_name: any;
  remark_data: any;
  total_paid: any;
  rep_diff: any;
  rep_diff2 : any;
  rest_debt:any;
  projcode: any;
  
  }
export interface _ipofcnotnull {
  hn : any;
  an: any;
  cid: any;
  fullname: any;
  acc_name: any;
  admitdate: any;
  dchdate: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  adjrw: any;
  total_summary: any;
  projcode: any;
  rep_diff: any;
  rep_diff2: any;
  receipt_no:any;
}

export interface _ipofcaccbydate {
  dchdate: any;
  allcase: any;
  debit: any;
  nullcase: any;
  nulldebit: any;
  notnullcase: any;
  notnulldebit: any;
  recieve: any;
  diff: any;
}
export interface _errorcode{
  error_code:any;
  error_name:any;
  counterrorcode:any;
}

@Component({
  selector: 'app-rep-ip-ofc',
  templateUrl: './rep-ip-ofc.component.html',
  styleUrls: ['./rep-ip-ofc.component.scss']
})
export class RepIpOfcComponent {
//count datatable
countRowstab1 :any;
countRowstab2 :any;
countRowstab3 :any;
countRowstab4 :any;
countRowstab5 :any;

//สร้างตัวแปร items,datestart,dateend

//itemerrorcode
itemerrorcode:any=[];
itemerrorcodeExecl:any=[];

//ipofcaccbydate
itemIpofcaccbydate : any =[];
itemsExeclIpofcaccbydate:any = [];

//ipofcnotNull
itemIpofcnotNull : any = [];
itemsExeclIpofcnotNull:any = [];

//ipofcNull
itemIpofcNull : any = [];
itemsExeclIpofcNull:any = [];

//accnull
ipofcaccnull_count : any=0;
ipofcaccnull_sum : any=0;

//debt_account
itemaccount:any = [];
selecteditemaccount:any='';

//รอดำเนินการมี repno แต่ไม่มี stm
ipofcaccnotnullstm_count : any=0;
ipofcaccnotnullstm_sum : any=0;

 //ipofcnotNull_stm
itemIpofcnotnull_stm : any = [];
itemsExeclIpofcNotNull_stm:any = [];

//accnotnull
all_notnullcase: any=0;
debit_notnull: any=0;
recieve: any=0;
sum_diff: any=0;
diffgain: any=0;
diffloss: any=0;
ipofccall : any =0;
ipofcsumdebit : any =0;

//circular
percent_null:any = 0;
percent_rep:any = 0;
percent_success:any = 0;

startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
isCheckbox : boolean = false;
// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
ipofcnulldisplayedColumns: string[] = ['hn','an','cid','fullname',
'acc_name', 'admitdate', 'dchdate','charge','paid','debt','repno','projcode','error_code','error_name'];

ipofcnotnullstmdisplayedColumns: string[] = ['hn','an','cid','fullname',
  'acc_name', 'admitdate', 'dchdate','charge','paid','debt','repno','projcode','error_code','error_name'];

ipofcnotnulldisplayedColumns: string[] = ['hn','an','cid','fullname',
'acc_name', 'admitdate', 'dchdate','charge','paid','debt','repno','projcode','adjrw',
'total_summary','rep_diff','rep_diff2','receipt_no'];

ipofcaccbydatedisplayedColumns: string[] = ['dchdate','allcase','debit','nullcase',
'nulldebit', 'notnullcase', 'notnulldebit','recieve','diff'];

errorcodedisplayedColumns: string[] = ['error_code','error_name','counterror'];


@ViewChild('paginator') paginator: MatPaginator;
@ViewChild('paginator2') paginator2: MatPaginator;
@ViewChild('paginator3') paginator3: MatPaginator;
@ViewChild('paginator4') paginator4: MatPaginator;
@ViewChild('paginator5') paginator5: MatPaginator;

ngAfterViewInit_ipofcnull() {
    this.itemIpofcNull.paginator = this.paginator;
  }
  ngAfterViewInit_ipofcNotnull_stm() {
    this.itemIpofcnotnull_stm.paginator = this.paginator2;
  }
  ngAfterViewInit_ipofcnotnull() {
    this.itemIpofcnotNull.paginator = this.paginator3;
  }
  ngAfterViewInit_ipofcaccbydate() {
    this.itemIpofcaccbydate.paginator = this.paginator4;
  }
  ngAfterViewInit_ipofcerrorcode() {
    this.itemerrorcode.paginator = this.paginator5;
  }

  constructor(
    private router: Router,
    private ipofcService:IpofcService,
    private excelService : ExcelService
) {} 

ngOnInit(): void {
  this.isCheckbox  = true;
  this.getLoadData()
  //this.getipofcacc()
}

//get DATA TABLE
async getLoadData(){
    await this.getLookup_debtaccount();
}
//get DATA Table debt_account of db HICM
async getLookup_debtaccount(){
  let lcACC :any = await this.ipofcService.getselect_debt_account_hi();
  console.log('lcACC',lcACC);
  
  this.itemaccount = lcACC;
}
async getipofcacc(){
 
  let lcCode:any = this.selecteditemaccount; //OFC
  let info: any = {
  "accType": lcCode,
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info:',info);
  //ผู้ป่วยส่งเบิกทั้งหมด OK
  await this.ipCounofcAll(info)
  //ดำเนินการเสร็จ OK
  await this.ipofcaccnotnull(info);
  //มี repno แต่ไม่มี statement
  await this.ipofcnotnull_stm(info);
  //รอดำเนินการ OK
  await this.ipofcaccnull(info);
    //มี repno แต่ไม่มี statement
    await this.ipofcnotnull_stm(info);

  //tab 1 ok
  await this.tab_ipofcnull(info);
  //tab 2 ok
  await this.tab_ipofcaccnotnull_stm(info);
  //tab 3 ok
  await this.tab_ipofcnotnull(info);
  //tab 4 ok
  await this.tab_ipofcaccbydate(info);
  //tab 5 ok
  await this.tab_ipofcbyerror(info);
  //Total Charge
  await this.ipofcall();
  //Percents
  await this.circularprogress()

}

async ipofcaccnull(info){
  try {
    let rs: any = await this.ipofcService.getipofcaccnull(info);
    console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.ipofcaccnull_count = rs.results.rows[0].count_an;
      this.ipofcaccnull_sum = rs.results.rows[0].sum_debt;
      console.log(this.ipofcaccnull_count);

    }
  } catch (error: any) {
      console.log();

  }

}

async ipofcnotnull_stm(info){
  try {
    let rs: any = await this.ipofcService.getipofcnotnull_stm(info);
    console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.ipofcaccnotnullstm_count = rs.results.rows[0].count_an;
      this.ipofcaccnotnullstm_sum = rs.results.rows[0].sum_debt;
      console.log(this.ipofcaccnotnullstm_sum);

    }
  } catch (error: any) {
      console.log();

  }

}
async ipofcaccnotnull(info){
  try {
    let rs: any = await this.ipofcService.getipofcaccnotnull(info);
    console.log('ofcaccnotull',rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.all_notnullcase = rs.results.rows[0].all_notnullcase;
      this.debit_notnull = rs.results.rows[0].debit_notnull;
      this.recieve = rs.results.rows[0].recieve;
      this.sum_diff = rs.results.rows[0].sum_diff;
      this.diffgain = rs.results.rows[0].diffgain;
      this.diffloss = rs.results.rows[0].diffloss;

    }
  } catch (error: any) {
      console.log();
  }

}
async ipCounofcAll(info){ 
  try {
    let rs: any = await this.ipofcService.getstmipCount(info);
    let data:any = rs.results;
    console.log('xx',rs);
    if (rs.results.ok){
      this.ipofccall = rs.results.rows[0].all_ip ;

    }
  } catch (error: any) {
      console.log(error);
  }
}

async ipofcall(){
      // this.ipofccall = +this.all_notnullcase + +this.ipofcaccnull_count    ipofcaccnull_sum ipofcaccnotnullstm_sum debit_notnull;
      this.ipofcsumdebit = +this.debit_notnull + +this.ipofcaccnull_sum + +this.ipofcaccnotnullstm_sum;
      //console.log('total',this.ipofcsumdebit);
      
}

async tab_ipofcnull(info){
  this.itemIpofcNull= [];
  this.itemsExeclIpofcNull=[];
  try {
      let rs: any = await this.ipofcService.getipofcnull(info);
      let data:any = rs.results.rows
      let xx:string = data
      this.countRowstab1 = xx.length
      
      if (rs.results.ok){
        this.itemsExeclIpofcNull = data;
        this.itemIpofcNull = new MatTableDataSource<_ipofcnull>(data);
        this.ngAfterViewInit_ipofcnull();

      }
    } catch (error: any) {
        console.log();
    }

  }
  async tab_ipofcaccnotnull_stm(info){
    this.itemIpofcnotnull_stm= [];
    this.itemsExeclIpofcNotNull_stm=[];
    try {
        let rs: any = await this.ipofcService.getipofcaccnotnull_stm(info);
        let data:any = rs.results.rows
        let xx:string = data
        console.log('xxxxx',xx);
        
        this.countRowstab5 = xx.length
        
        if (rs.results.ok){
          this.itemsExeclIpofcNotNull_stm = data;
          this.itemIpofcnotnull_stm = new MatTableDataSource<_ipofcnotnull_stm>(data);
          this.ngAfterViewInit_ipofcNotnull_stm();
  
        }
      } catch (error: any) {
          console.log();
      }
  
    }
  
  async tab_ipofcnotnull(info){
      this.itemIpofcnotNull= [];
      this.itemsExeclIpofcnotNull=[];
      try {
          let rs: any = await this.ipofcService.getipofcnotnull(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab2 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclIpofcnotNull = data;
            this.itemIpofcnotNull = new MatTableDataSource<_ipofcnotnull>(data);
            this.ngAfterViewInit_ipofcnotnull();

          }
        } catch (error: any) {
            console.log();
        }
  }

  async tab_ipofcaccbydate(info){
      this.itemIpofcaccbydate= [];
      this.itemsExeclIpofcaccbydate=[];
      try {
          let rs: any = await this.ipofcService.getipofcaccbydate(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab3 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclIpofcaccbydate = data;
            this.itemIpofcaccbydate = new MatTableDataSource<_ipofcaccbydate>(data);
            this.ngAfterViewInit_ipofcaccbydate();

          }
        } catch (error: any) {
            console.log();
        }

  }
  async tab_ipofcbyerror(info){
    this.itemerrorcode= [];
    this.itemerrorcodeExecl=[];
    try {
        let rs: any = await this.ipofcService.gettoperrorcode(info);
        let data:any = rs.results.rows;
        let xx:string = data
        this.countRowstab4 = xx.length
        // console.log(rs);
        if (rs.results.ok){
          this.itemerrorcodeExecl = data;
          this.itemerrorcode = new MatTableDataSource<_errorcode>(data);
          this.ngAfterViewInit_ipofcerrorcode();

        }
      } catch (error: any) {
          console.log();
      }

}
      exportAsXLSXNull():void {
          this.excelService.exportAsExcelFile(this.itemsExeclIpofcNull, 'ExporttoExcel');
        }
        exportAsXLSXNotNull_stm():void {
          this.excelService.exportAsExcelFile(this.itemsExeclIpofcNotNull_stm, 'ExporttoExcel');
        }
      exportAsXLSXnotNull():void {
       this.excelService.exportAsExcelFile(this.itemsExeclIpofcnotNull, 'ExporttoExcel');
     }

     exportAsXLSXAccbyDate():void {
      this.excelService.exportAsExcelFile(this.itemsExeclIpofcaccbydate, 'ExporttoExcel');
    }
    exportAsXLSXErrorcode():void {
      this.excelService.exportAsExcelFile(this.itemerrorcode, 'ExporttoExcel');
    }
    //get debt_account
    onOptionsSelected(){
      this.isCheckbox  = false;
      console.log('ACC',this.selecteditemaccount);
      
    }
    logCheckbox() {
      if(this.isCheckbox === true) {
        this.selecteditemaccount = '';
      }
      console.log(this.isCheckbox);
    }

    _setDataSource(indexNumber) {
      setTimeout(() => {
        switch (indexNumber) {
          case 0:
            !this.itemIpofcNull.paginator ? this.itemIpofcNull.paginator = this.paginator : null;
            break;
          case 1:
              !this.itemsExeclIpofcNotNull_stm.paginator ? this.itemsExeclIpofcNotNull_stm.paginator = this.paginator2 : null;  
              break;
           case 2:
            !this.itemIpofcnotNull.paginator ? this.itemIpofcnotNull.paginator = this.paginator3 : null;
            break;
          case 3:
            !this.itemIpofcaccbydate.paginator ? this.itemIpofcaccbydate.paginator = this.paginator4 : null;
            break;
          case 4:
            !this.itemerrorcode.paginator ? this.itemerrorcode.paginator = this.paginator5 : null;
        }
      });
    }

circularprogress(){
  this.percent_null =  Math.round(this.ipofcaccnull_count * 100 / this.ipofccall)
  this.percent_rep = Math.round(this.ipofcaccnotnullstm_count * 100 / this.ipofccall)
  this.percent_success = Math.round(this.all_notnullcase * 100 / this.ipofccall)
  console.log('rep_percent',this.percent_null);
  
}

}
