import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';


import { RepIpOfcRoutingModule } from './rep-ip-ofc-routing.module';
import { RepIpOfcComponent } from './rep-ip-ofc.component';
import { ExcelService } from 'app/services/excel.service';

import { CircularProgressComponent } from '../../../components/circular-progress/circular-progress.component'

@NgModule({
  declarations: [
    RepIpOfcComponent
  ],
  providers:[ExcelService],
  imports: [
    RepIpOfcRoutingModule,
    MatPaginatorModule,
    SharedModule,
    CircularProgressComponent,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class RepIpOfcModule { }
