import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {RepIpStpComponent} from './rep-ip-stp.component';
const routes: Routes = [
  {
    path:'',component:RepIpStpComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepIpStpRoutingModule { }
