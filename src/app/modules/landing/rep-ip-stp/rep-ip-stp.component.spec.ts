import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepIpStpComponent } from './rep-ip-stp.component';

describe('RepIpStpComponent', () => {
  let component: RepIpStpComponent;
  let fixture: ComponentFixture<RepIpStpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepIpStpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepIpStpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
