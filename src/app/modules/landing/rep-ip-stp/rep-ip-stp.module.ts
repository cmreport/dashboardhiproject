import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { RepIpStpRoutingModule } from './rep-ip-stp-routing.module';
import { RepIpStpComponent } from './rep-ip-stp.component';

import { ExcelService } from 'app/services/excel.service';
import { CircularProgressComponent } from '../../../components/circular-progress/circular-progress.component'

@NgModule({
  declarations: [
    RepIpStpComponent
  ],
  providers:[ExcelService],
  imports: [
    RepIpStpRoutingModule,
    MatPaginatorModule,
    SharedModule,
    CircularProgressComponent,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class RepIpStpModule { }
