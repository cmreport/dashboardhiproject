import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/rep-ip-ucs/rep-ip-ucs.model'

import { IpucsService } from '../../../services/ipucs.service'

export interface _ipucsnull {
  an: any;
  hn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  admitdate: any;
  dchdate: any;
  l_stay: any;
  charge: any;
  paid: any;
  debt: any;
  debt2: any;
  repno: any;
  error_code: any;
  error_name: any;
  remark_data: any;
  total_paid: any;
  rep_diff: any;
  rep_diff2: any;
  rest_debt: any;
  projcode: any;
}

export interface _ipucsnotnull_stm {

  an: any;
  hn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  admitdate: any;
  dchdate: any;
  l_stay: any;
  charge: any;
  paid: any;
  debt: any;
  debt2: any;
  repno: any;
  error_code: any;
  error_name: any;
  remark_data: any;
  total_paid: any;
  rep_diff: any;
  rep_diff2: any;
  rest_debt: any;
  projcode: any;

}

export interface _ipucsnotnull {
  hn: any;
  an: any;
  cid: any;
  fullname: any;
  acc_name: any;
  admitdate: any;
  dchdate: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  adjrw: any;
  total_summary: any;
  error_code: any;
  error_name: any;
  projcode: any;
  rep_diff: any;
  rep_diff2: any;
  receipt_no: any;
}

export interface _ipucsaccbydate {
  dchdate: any;
  allcase: any;
  debit: any;
  nullcase: any;
  nulldebit: any;
  notnullcase: any;
  notnulldebit: any;
  recieve: any;
  diff: any;
  error_code: any;
  error_name: any;
}

export interface _errorcode {
  error_code: any;
  error_name: any;
  counterrorcode: any;
}

@Component({
  selector: 'app-rep-ip-ucs',
  templateUrl: './rep-ip-ucs.component.html',
  styleUrls: ['./rep-ip-ucs.component.scss']
})
export class RepIpUcsComponent {

  //count datatable
  countRowstab1: any;
  countRowstab2: any;
  countRowstab3: any;
  countRowstab4: any;
  countRowstab5: any;

  //สร้างตัวแปร items,datestart,dateend
  //itemerrorcode
  itemerrorcode: any = [];
  itemerrorcodeExecl: any = [];

  //ipucsaccbydate
  itemIpucsaccbydate: any = [];
  itemsExeclIpucsaccbydate: any = [];

  //ipucsnotNull
  itemIpucsnotNull: any = [];
  itemsExeclIpucsnotNull: any = [];

  //ipucsNull
  itemIpucsNull: any = [];
  itemsExeclIpucsNull: any = [];

  //accnull
  ipucsaccnull_count: any = 0;
  ipucsaccnull_sum: any = 0;

  //debt_account
  itemaccount: any = [];
  selecteditemaccount: any = '';

  //รอดำเนินการมี repno แต่ไม่มี stm
  ipucsaccnotnullstm_count: any = 0;
  ipucsaccnotnullstm_sum: any = 0;

  //ipofcnotNull_stm
  itemIpucsnotnull_stm: any = [];
  itemsExeclIpucsNotNull_stm: any = [];

  //accnotnull
  all_notnullcase: any = 0;
  debit_notnull: any = 0;
  recieve: any = 0;
  sum_diff: any = 0;
  diffgain: any = 0;
  diffloss: any = 0;
  ipucscall: any = 0;
  ipucssumdebit: any = 0;

  //circular
  percent_null: any = 0;
  percent_rep: any = 0;
  percent_success: any = 0;

  startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  isCheckbox: boolean = false;

  // ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
  ipucsnulldisplayedColumns: string[] = ['hn', 'an', 'cid', 'fullname',
    'acc_name', 'admitdate', 'dchdate', 'charge', 'paid', 'debt', 'repno', 'projcode', 'error_code', 'error_name'];

  ipucsnotnullstmdisplayedColumns: string[] = ['hn', 'an', 'cid', 'fullname',
    'acc_name', 'admitdate', 'dchdate', 'charge', 'paid', 'debt', 'repno', 'projcode', 'error_code', 'error_name'];

  ipucsnotnulldisplayedColumns: string[] = ['hn', 'an', 'cid', 'fullname',
    'acc_name', 'admitdate', 'dchdate', 'charge', 'paid', 'debt', 'repno', 'projcode', 'adjrw',
    'total_summary', 'rep_diff', 'rep_diff2', 'receipt_no'];

  ipucsaccbydatedisplayedColumns: string[] = ['dchdate', 'allcase', 'debit', 'nullcase',
    'nulldebit', 'notnullcase', 'notnulldebit', 'recieve', 'diff'];

  errorcodedisplayedColumns: string[] = ['error_code', 'error_name', 'counterror'];

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginator2') paginator2: MatPaginator;
  @ViewChild('paginator3') paginator3: MatPaginator;
  @ViewChild('paginator4') paginator4: MatPaginator;
  @ViewChild('paginator5') paginator5: MatPaginator;


  ngAfterViewInit_ipucsnull() {
    this.itemIpucsNull.paginator = this.paginator;
  }
  ngAfterViewInit_ipucsNotnull_stm() {
    this.itemIpucsnotnull_stm.paginator = this.paginator2;
  }
  ngAfterViewInit_ipucsnotnull() {
    this.itemIpucsnotNull.paginator = this.paginator3;
  }
  ngAfterViewInit_ipucsaccbydate() {
    this.itemIpucsaccbydate.paginator = this.paginator4;
  }
  ngAfterViewInit_ipucserrorcode() {
    this.itemerrorcode.paginator = this.paginator5;
  }

  constructor(
    private router: Router,
    private ipucsService: IpucsService,
    private excelService: ExcelService
  ) { }


  ngOnInit(): void {
    this.isCheckbox = true;
    this.getLoadData()
    //this.getipucsacc()
  }

  //get DATA TABLE
  async getLoadData() {
    await this.getLookup_debtaccount();
  }
  //get DATA Table debt_account of db HICM
  async getLookup_debtaccount() {
    let lcACC: any = await this.ipucsService.getselect_debt_account_hi();
    console.log('lcACC', lcACC);

    this.itemaccount = lcACC;
  }
  async getipucsacc() {

    let lcCode: any = this.selecteditemaccount; //ucs
    let info: any = {
      "accType": lcCode,
      "startDate": moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
      "endDate": moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
    }
    console.log('info:', info);
    //ผู้ป่วยส่งเบิกทั้งหมด OK
    await this.ipCountucsAll(info)
    //ดำเนินการเสร็จ OK
    await this.ipucsaccnotnull(info);
    //รอดำเนินการ OK
    await this.ipucsaccnull(info);
    //มี repno แต่ไม่มี statement
    await this.ipucsnotnull_stm(info);

    //tab 1 ok
    await this.tab_ipucsnull(info);
    //tab 2 ok
    await this.tab_ipucsaccnotnull_stm(info);
    //tab 3 ok
    await this.tab_ipucsnotnull(info);
    //tab 4 ok
    await this.tab_ipucsaccbydate(info);
    //tab 5 ok 
    await this.tab_ipucsbyerror(info);
    //Sum total
    await this.ipucsall();
    //Percents
    await this.circularprogress();
  }
  async ipucsnotnull_stm(info) {
    try {
      let rs: any = await this.ipucsService.getipucsnotnull_stm(info);
      console.log(rs);
      if (rs.results.ok) {
        // this.itemsExecl = rs;
        this.ipucsaccnotnullstm_count = rs.results.rows[0].count_an;
        this.ipucsaccnotnullstm_sum = rs.results.rows[0].sum_debt;
        console.log(this.ipucsaccnotnullstm_sum);

      }
    } catch (error: any) {
      console.log();

    }

  }

  async tab_ipucsaccnotnull_stm(info) {
    this.itemIpucsnotnull_stm = [];
    this.itemsExeclIpucsNotNull_stm = [];
    try {
      let rs: any = await this.ipucsService.getipucsaccnotnull_stm(info);
      let data: any = rs.results.rows
      let xx: string = data
      console.log('xxxxx', xx);

      this.countRowstab5 = xx.length

      if (rs.results.ok) {
        this.itemsExeclIpucsNotNull_stm = data;
        this.itemIpucsnotnull_stm = new MatTableDataSource<_ipucsnotnull_stm>(data);
        this.ngAfterViewInit_ipucsNotnull_stm();

      }
    } catch (error: any) {
      console.log();
    }

  }

  async ipCountucsAll(info) {
    try {
      let rs: any = await this.ipucsService.getstmipCount(info);
      let data: any = rs.results;
      console.log('xx', rs);
      if (rs.results.ok) {
        this.ipucscall = rs.results.rows[0].all_ip;

      }
    } catch (error: any) {
      console.log(error);
    }
  }

  async ipucsaccnull(info) {
    try {
      let rs: any = await this.ipucsService.getipucsaccnull(info);
      console.log(rs);
      if (rs.results.ok) {
        // this.itemsExecl = rs;
        this.ipucsaccnull_count = rs.results.rows[0].count_an;
        this.ipucsaccnull_sum = rs.results.rows[0].sum_debt;
        console.log(this.ipucsaccnull_count);

      }
    } catch (error: any) {
      console.log();

    }

  }
  async ipucsaccnotnull(info) {
    try {
      let rs: any = await this.ipucsService.getipucsaccnotnull(info);
      console.log(rs);
      if (rs.results.ok) {
        // this.itemsExecl = rs;
        this.all_notnullcase = rs.results.rows[0].all_notnullcase;
        this.debit_notnull = rs.results.rows[0].debit_notnull;
        this.recieve = rs.results.rows[0].recieve;
        this.sum_diff = rs.results.rows[0].sum_diff;
        this.diffgain = rs.results.rows[0].diffgain;
        this.diffloss = rs.results.rows[0].diffloss;

      }
    } catch (error: any) {
      console.log();
    }

  }

  async ipucsall() {
    // this.ipofccall = +this.all_notnullcase + +this.ipofcaccnull_count    ipofcaccnull_sum ipofcaccnotnullstm_sum debit_notnull;
    this.ipucssumdebit = +this.debit_notnull + +this.ipucsaccnull_sum + +this.ipucsaccnotnullstm_sum;
    //console.log('total',this.ipofcsumdebit);

  }
  async tab_ipucsnull(info) {
    this.itemIpucsNull = [];
    this.itemsExeclIpucsNull = [];
    try {
      let rs: any = await this.ipucsService.getipucsnull(info);
      let data: any = rs.results.rows;
      let xx: string = data
      this.countRowstab1 = xx.length
      // console.log(rs);
      if (rs.results.ok) {
        this.itemsExeclIpucsNull = data;
        this.itemIpucsNull = new MatTableDataSource<_ipucsnull>(data);
        this.ngAfterViewInit_ipucsnull();

      }
    } catch (error: any) {
      console.log();
    }

  }

  async tab_ipucsnotnull(info) {
    this.itemIpucsnotNull = [];
    this.itemsExeclIpucsnotNull = [];
    try {
      let rs: any = await this.ipucsService.getipucsnotnull(info);
      let data: any = rs.results.rows;
      let xx: string = data
      this.countRowstab2 = xx.length
      // console.log(rs);
      if (rs.results.ok) {
        this.itemsExeclIpucsnotNull = data;
        this.itemIpucsnotNull = new MatTableDataSource<_ipucsnotnull>(data);
        this.ngAfterViewInit_ipucsnotnull();

      }
    } catch (error: any) {
      console.log();
    }
  }

  async tab_ipucsaccbydate(info) {
    this.itemIpucsaccbydate = [];
    this.itemsExeclIpucsaccbydate = [];
    try {
      let rs: any = await this.ipucsService.getipucsaccbydate(info);
      let data: any = rs.results.rows;
      let xx: string = data
      this.countRowstab3 = xx.length
      // console.log(rs);
      if (rs.results.ok) {
        this.itemsExeclIpucsaccbydate = data;
        this.itemIpucsaccbydate = new MatTableDataSource<_ipucsaccbydate>(data);
        this.ngAfterViewInit_ipucsaccbydate();

      }
    } catch (error: any) {
      console.log();
    }

  }
  async tab_ipucsbyerror(info) {
    this.itemerrorcode = [];
    this.itemerrorcodeExecl = [];
    try {
      let rs: any = await this.ipucsService.gettoperrorcode(info);
      let data: any = rs.results.rows;
      let xx: string = data
      this.countRowstab4 = xx.length
      // console.log(rs);
      if (rs.results.ok) {
        this.itemerrorcodeExecl = data;
        this.itemerrorcode = new MatTableDataSource<_errorcode>(data);
        this.ngAfterViewInit_ipucserrorcode();

      }
    } catch (error: any) {
      console.log();
    }

  }
  exportAsXLSXNull(): void {
    this.excelService.exportAsExcelFile(this.itemsExeclIpucsNull, 'ExporttoExcel');
  }
  exportAsXLSXNotNull_stm(): void {
    this.excelService.exportAsExcelFile(this.itemsExeclIpucsNotNull_stm, 'ExporttoExcel');
  }
  exportAsXLSXnotNull(): void {
    this.excelService.exportAsExcelFile(this.itemsExeclIpucsnotNull, 'ExporttoExcel');
  }

  exportAsXLSXAccbyDate(): void {
    this.excelService.exportAsExcelFile(this.itemsExeclIpucsaccbydate, 'ExporttoExcel');
  }

  exportAsXLSXErrorcode(): void {
    this.excelService.exportAsExcelFile(this.itemerrorcode, 'ExporttoExcel');
  }
  //get debt_account
  onOptionsSelected() {
    this.isCheckbox = false;
    console.log('ACC', this.selecteditemaccount);

  }
  logCheckbox() {
    if (this.isCheckbox === true) {
      this.selecteditemaccount = '';
    }
    console.log(this.isCheckbox);
  }
  _setDataSource(indexNumber) {
    setTimeout(() => {
      switch (indexNumber) {
        case 0:
          !this.itemIpucsNull.paginator ? this.itemIpucsNull.paginator = this.paginator : null;
          break;
        case 1:
          !this.itemsExeclIpucsNotNull_stm.paginator ? this.itemsExeclIpucsNotNull_stm.paginator = this.paginator2 : null;
          break;
        case 2:
          !this.itemIpucsnotNull.paginator ? this.itemIpucsnotNull.paginator = this.paginator3 : null;
          break;
        case 3:
          !this.itemIpucsaccbydate.paginator ? this.itemIpucsaccbydate.paginator = this.paginator4 : null;
          break;
        case 4:
          !this.itemerrorcode.paginator ? this.itemerrorcode.paginator = this.paginator5 : null;
      }
    });
  }
  circularprogress() {
    this.percent_null = Math.round(this.ipucsaccnull_count * 100 / this.ipucscall)
    this.percent_rep = Math.round(this.ipucsaccnotnullstm_count * 100 / this.ipucscall)
    this.percent_success = Math.round(this.all_notnullcase * 100 / this.ipucscall)
    console.log('rep_percent', this.percent_null);

  }

}