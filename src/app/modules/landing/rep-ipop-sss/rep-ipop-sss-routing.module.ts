import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepIpopSssComponent } from './rep-ipop-sss.component';

const routes: Routes = [
  {
    path:'',component:RepIpopSssComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepIpopSssRoutingModule { }
