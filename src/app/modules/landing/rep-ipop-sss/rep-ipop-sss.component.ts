import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/rep-ipop-sss/rep-ipop-sss.model'

import { IpsssService } from '../../../services/ipsss.service'

export interface _ipsssnull {
  an: any;
  hn: any;
  cid: any;
  fullname: any;
  acc_name:any;
  admitdate: any;
  dchdate: any;
  l_stay: any;
  charge: any;
  paid: any;
  debt: any;
  debt2: any;
  repno: any;
  error_code: any;
  error_name: any;
  remark_data: any;
  total_paid: any;
  rep_diff: any;
  rep_diff2 : any;
  rest_debt:any;
  projcode: any;
}
export interface _ipsssnotnull {
  hn : any;
  an: any;
  cid: any;
  fullname: any;
  acc_name: any;
  admitdate: any;
  dchdate: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  adjrw: any;
  total_summary: any;
  projcode: any;
}

export interface _ipsssaccbydate {
  dchdate: any;
  allcase: any;
  debit: any;
  nullcase: any;
  nulldebit: any;
  notnullcase: any;
  notnulldebit: any;
  recieve: any;
  diff: any;
}
export interface _errorcode{
  error_code:any;
  error_name:any;
  counterrorcode:any;
}
@Component({
  selector: 'app-rep-ipop-sss',
  templateUrl: './rep-ipop-sss.component.html',
  styleUrls: ['./rep-ipop-sss.component.scss']
})
export class RepIpopSssComponent {

//count datatable
countRowstab1 :any;
countRowstab2 :any;
countRowstab3 :any;
countRowstab4 :any;

//สร้างตัวแปร items,datestart,dateend
//itemerrorcode
itemerrorcode:any=[];
itemerrorcodeExecl:any=[];

//ipsssaccbydate
itemIpsssaccbydate : any =[];
itemsExeclIpsssaccbydate:any = [];

//ipsssnotNull
itemIpsssnotNull : any = [];
itemsExeclIpsssnotNull:any = [];

//ipsssNull
itemIpsssNull : any = [];
itemsExeclIpsssNull:any = [];

//accnull
ipsssaccnull_count : any=0;
ipsssaccnull_sum : any=0;

 //debt_account
 itemaccount:any = [];
 selecteditemaccount:any='';

//accnotnull
all_notnullcase: any=0;
debit_notnull: any=0;
recieve: any=0;
sum_diff: any=0;
diffgain: any=0;
diffloss: any=0;
ipssscall : any =0;
ipssssumdebit : any =0;

startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
isCheckbox : boolean = false;

// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
ipsssnulldisplayedColumns: string[] = ['hn','an','cid','fullname',
'acc_name', 'admitdate', 'dchdate','charge','paid','debt','repno','projcode','error_code','error_name'];

ipsssnotnulldisplayedColumns: string[] = ['hn','an','cid','fullname',
'acc_name', 'admitdate', 'dchdate','charge','paid','debt','repno','projcode','adjrw',
'total_summary'];

ipsssaccbydatedisplayedColumns: string[] = ['dchdate','allcase','debit','nullcase',
'nulldebit', 'notnullcase', 'notnulldebit','recieve','diff'];

errorcodedisplayedColumns: string[] = ['error_code','error_name','counterror'];


@ViewChild('paginator') paginator: MatPaginator;
@ViewChild('paginator2') paginator2: MatPaginator;
@ViewChild('paginator3') paginator3: MatPaginator;
@ViewChild('paginator4') paginator4: MatPaginator;

ngAfterViewInit_ipsssnull() {
    this.itemIpsssNull.paginator = this.paginator;
  }
  ngAfterViewInit_ipsssnotnull() {
    this.itemIpsssnotNull.paginator = this.paginator2;
  }
  ngAfterViewInit_ipsssaccbydate() {
    this.itemIpsssaccbydate.paginator = this.paginator3;
  }
  ngAfterViewInit_ipssserrorcode() {
    this.itemerrorcode.paginator = this.paginator4;
  }
  constructor(
    private router: Router,
    private ipsssService:IpsssService,
    private excelService : ExcelService
) {} 


ngOnInit(): void {
  this.isCheckbox  = true;
  this.getLoadData()
  //this.getipsssacc()
}

//get DATA TABLE
async getLoadData(){
    await this.getLookup_debtaccount();
}
//get DATA Table debt_account of db HICM
async getLookup_debtaccount(){
  let lcACC :any = await this.ipsssService.getselect_debt_account_hi();
  console.log('lcACC',lcACC);
  
  this.itemaccount = lcACC;
}
async getipsssacc(){

  let lcCode:any = this.selecteditemaccount; //sss
  let info: any = {
  "accType": lcCode,
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info:',info);
   //ผู้ป่วยส่งเบิกทั้งหมด OK
   await this.ipCountsssAll(info)
   //ดำเนินการเสร็จ OK
   await this.ipsssaccnotnull(info);
   //รอดำเนินการ OK
   await this.ipsssaccnull(info);
   // tab 1O K
   await this.tab_ipsssnull(info);
   //tab 2 ok
   await this.tab_ipsssnotnull(info);
   //tab 3 ok
   await this.tab_ipsssaccbydate(info);
   //tab 4 ok
   await this.tab_ipsssbyerror(info);   
 }
 async ipCountsssAll(info){ 
   try {
     let rs: any = await this.ipsssService.getstmipCount(info);
     let data:any = rs.results;
     console.log('xx',rs);
     if (rs.results.ok){
       this.ipssscall = rs.results.rows[0].all_ip ;
 
     }
   } catch (error: any) {
       console.log(error);
   }
 }
async ipsssaccnull(info){
  try {
    let rs: any = await this.ipsssService.getipsssaccnull(info);
    console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.ipsssaccnull_count = rs.results.rows[0].count_an;
      this.ipsssaccnull_sum = rs.results.rows[0].sum_debt;
      console.log(this.ipsssaccnull_count);

    }
  } catch (error: any) {
      console.log();

  }

}
async ipsssaccnotnull(info){
  try {
    let rs: any = await this.ipsssService.getipsssaccnotnull(info);
    console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.all_notnullcase = rs.results.rows[0].all_notnullcase;
      this.debit_notnull = rs.results.rows[0].debit_notnull;
      this.recieve = rs.results.rows[0].recieve;
      this.sum_diff = rs.results.rows[0].sum_diff;
      this.diffgain = rs.results.rows[0].diffgain;
      this.diffloss = rs.results.rows[0].diffloss;

    }
  } catch (error: any) {
      console.log();
  }

}

async ipsssall(){
      // this.ipssscall = +this.all_notnullcase + +this.ipsssaccnull_count;
      this.ipssssumdebit = +this.debit_notnull + +this.ipsssaccnull_sum;
}

async tab_ipsssnull(info){
  this.itemIpsssNull= [];
  this.itemsExeclIpsssNull=[];
  try {
      let rs: any = await this.ipsssService.getipsssnull(info);
      let data:any = rs.results.rows;
      let xx:string = data
      this.countRowstab1 = xx.length
      // console.log(rs);
      if (rs.results.ok){
        this.itemsExeclIpsssNull = data;
        this.itemIpsssNull = new MatTableDataSource<_ipsssnull>(data);
        this.ngAfterViewInit_ipsssnull();

      }
    } catch (error: any) {
        console.log();
    }

  }

  async tab_ipsssnotnull(info){
      this.itemIpsssnotNull= [];
      this.itemsExeclIpsssnotNull=[];
      try {
          let rs: any = await this.ipsssService.getipsssnotnull(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab2 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclIpsssnotNull = data;
            this.itemIpsssnotNull = new MatTableDataSource<_ipsssnotnull>(data);
            this.ngAfterViewInit_ipsssnotnull();

          }
        } catch (error: any) {
            console.log();
        }
  }

  async tab_ipsssaccbydate(info){
      this.itemIpsssaccbydate= [];
      this.itemsExeclIpsssaccbydate=[];
      try {
          let rs: any = await this.ipsssService.getipsssaccbydate(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab3 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclIpsssaccbydate = data;
            this.itemIpsssaccbydate = new MatTableDataSource<_ipsssaccbydate>(data);
            this.ngAfterViewInit_ipsssaccbydate();

          }
        } catch (error: any) {
            console.log();
        }

  }
  async tab_ipsssbyerror(info){
    this.itemerrorcode= [];
    this.itemerrorcodeExecl=[];
    try {
        let rs: any = await this.ipsssService.gettoperrorcode(info);
        let data:any = rs.results.rows;
        let xx:string = data
        this.countRowstab4 = xx.length
        // console.log(rs);
        if (rs.results.ok){
          this.itemerrorcodeExecl = data;
          this.itemerrorcode = new MatTableDataSource<_errorcode>(data);
          this.ngAfterViewInit_ipssserrorcode();

        }
      } catch (error: any) {
          console.log();
      }

}
      exportAsXLSXNull():void {
          this.excelService.exportAsExcelFile(this.itemsExeclIpsssNull, 'ExporttoExcel');
        }

      exportAsXLSXnotNull():void {
       this.excelService.exportAsExcelFile(this.itemsExeclIpsssnotNull, 'ExporttoExcel');
     }

     exportAsXLSXAccbyDate():void {
      this.excelService.exportAsExcelFile(this.itemsExeclIpsssaccbydate, 'ExporttoExcel');
    }
    exportAsXLSXErrorcode():void {
      this.excelService.exportAsExcelFile(this.itemerrorcode, 'ExporttoExcel');
    }
    
    //get debt_account
    onOptionsSelected(){
      this.isCheckbox  = false;
      console.log('ACC',this.selecteditemaccount);
      
    }
    logCheckbox() {
      if(this.isCheckbox === true) {
        this.selecteditemaccount = '';
      }
      console.log(this.isCheckbox);
    }

    _setDataSource(indexNumber) {
      setTimeout(() => {
        switch (indexNumber) {
          case 0:
            !this.itemIpsssNull.paginator ? this.itemIpsssNull.paginator = this.paginator : null;
            break;
          case 1:
            !this.itemIpsssnotNull.paginator ? this.itemIpsssnotNull.paginator = this.paginator2 : null;
            break;
          case 2:
            !this.itemIpsssaccbydate.paginator ? this.itemIpsssaccbydate.paginator = this.paginator3 : null;
            break;
          case 3:
            !this.itemerrorcode.paginator ? this.itemerrorcode.paginator = this.paginator4 : null;
        }
      });
    }
}