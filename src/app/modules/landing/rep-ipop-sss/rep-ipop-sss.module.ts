import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { RepIpopSssRoutingModule } from './rep-ipop-sss-routing.module';
import { RepIpopSssComponent } from './rep-ipop-sss.component';

import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    RepIpopSssComponent
  ],
  providers:[ExcelService],
  imports: [
    RepIpopSssRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class RepIpopSssModule { }
