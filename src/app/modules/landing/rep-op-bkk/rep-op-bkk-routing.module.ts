import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepOpBkkComponent} from './rep-op-bkk.component';
const routes: Routes = [
  {
    path:'',component:RepOpBkkComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepOpBkkRoutingModule { }
