import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepOpBkkComponent } from './rep-op-bkk.component';

describe('RepOpBkkComponent', () => {
  let component: RepOpBkkComponent;
  let fixture: ComponentFixture<RepOpBkkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepOpBkkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepOpBkkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
