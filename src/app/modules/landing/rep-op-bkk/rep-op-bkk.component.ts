import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/rep-op-bkk/rep-op-bkk.model'


import { OpbkkService } from '../../../services/opbkk.service'

export interface _opbkknull {
  hn : any;
  vn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  visit_date: any;
  visit_time: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  error_code: any;
  error_name: any;
  total_summary: any;
  projcode: any;
}
export interface _opbkknotnull {
  hn : any;
  vn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  visit_date: any;
  visit_time: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  error_code: any;
  error_name: any;
  total_summary: any;
  projcode: any;
}

export interface _opbkkaccbydate {
  visit_date: any;
  allcase: any;
  debit: any;
  nullcase: any;
  nulldebit: any;
  notnullcase: any;
  notnulldebit: any;
  recieve: any;
  diff: any;
}

export interface _errorcode{
  error_code:any;
  error_name:any;
  counterrorcode:any;
}

@Component({
  selector: 'app-rep-op-bkk',
  templateUrl: './rep-op-bkk.component.html',
  styleUrls: ['./rep-op-bkk.component.scss']
})
export class RepOpBkkComponent {
//count datatable
countRowstab1 :any;
countRowstab2 :any;
countRowstab3 :any;
countRowstab4 :any;

//สร้างตัวแปร items,datestart,dateend
//itemerrorcode
itemerrorcode:any=[];
itemerrorcodeExecl:any=[];


//opbkkaccbydate
itemopbkkaccbydate : any =[];
itemsExeclopbkkaccbydate:any = [];

//opbkknotNull
itemopbkknotNull : any = [];
itemsExeclopbkknotNull:any = [];

//opbkkNull
itemopbkkNull : any = [];
itemsExeclopbkkNull:any = [];

//accnull
opbkkaccnull_count : any=0;
opbkkaccnull_sum : any=0;

 //debt_account
 itemaccount:any = [];
 selecteditemaccount:any='';

//accnotnull
all_notnullcase: any=0;
debit_notnull: any=0;
recieve: any=0;
sum_diff: any=0;
diffgain: any=0;
diffloss: any=0;
opbkkcall : any =0;
opbkksumdebit : any =0;

startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
isCheckbox : boolean = false;
// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
opbkknulldisplayedColumns: string[] = ['vn','hn','cid','fullname',
'acc_name', 'visit_date','visit_time','charge','paid','debt','repno','projcode','error_code','error_name','total_summary'];
opbkknotnulldisplayedColumns: string[] = ['vn','hn','cid','fullname',
'acc_name', 'visit_date','visit_time','charge','paid','debt','repno','projcode','error_code','error_name','total_summary'];

opbkkaccbydatedisplayedColumns: string[] = ['visit_date','allcase','debit','nullcase',
'nulldebit', 'notnullcase', 'notnulldebit','recieve','diff'];

errorcodedisplayedColumns: string[] = ['error_code','error_name','counterror'];


@ViewChild('paginator') paginator: MatPaginator;
@ViewChild('paginator2') paginator2: MatPaginator;
@ViewChild('paginator3') paginator3: MatPaginator;
@ViewChild('paginator4') paginator4: MatPaginator;

ngAfterViewInit_opbkknull() {
    this.itemopbkkNull.paginator = this.paginator;
  }
  ngAfterViewInit_opbkknotnull() {
    this.itemopbkknotNull.paginator = this.paginator2;
  }
  ngAfterViewInit_opbkkaccbydate() {
    this.itemopbkkaccbydate.paginator = this.paginator3;
  }
  ngAfterViewInit_opbkkerrorcode() {
    this.itemerrorcode.paginator = this.paginator4;
  }

  constructor(
    private router: Router,
    private opbkkService:OpbkkService,
    private excelService : ExcelService
) {} 


ngOnInit(): void {
  this.isCheckbox  = true;
  this.getLoadData()
  //this.getopofcacc()
}

//get DATA TABLE
async getLoadData(){
    await this.getLookup_debtaccount();
}
//get DATA Table debt_account of db HICM
async getLookup_debtaccount(){
  let lcACC :any = await this.opbkkService.getselect_debt_account_hi();
  console.log('lcACC',lcACC);
  
  this.itemaccount = lcACC;
}
async getopbkkacc(){

  let lcCode:any = this.selecteditemaccount; //OFC
  let info: any = {
  "accType": lcCode,
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info:',info);
  
//ผู้ป่วยส่งเบิกทั้งหมด OK
await this.opCountofcAll(info)
//ดำเนินการเสร็จ OK 
await this.opbkkaccnotnull(info);
//รอดำเนินการ OK 
await this.opbkkaccnull(info);
//tab 1 ok
await this.tab_opbkknull(info);
//tab 2 ok
await this.tab_opbkknotnull(info);
//tab 3 ok
await this.tab_opbkkaccbydate(info);
 //tab 4 
 await this.tab_opbkkbyerror(info);
}

async opCountofcAll(info){ 
try {
  let rs: any = await this.opbkkService.getstmopCount(info);
  let data:any = rs.results;
  console.log('xx',rs);
  if (rs.results.ok){
    this.opbkkcall = rs.results.rows[0].all_op ;

  }
} catch (error: any) {
    console.log(error);
}
}
async opbkkaccnull(info){
  try {
    let rs: any = await this.opbkkService.getopbkkaccnull(info);
    // console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.opbkkaccnull_count = rs.results.rows[0].count_vn;
      this.opbkkaccnull_sum = rs.results.rows[0].sum_debt;
      console.log(this.opbkkaccnull_count);

    }
  } catch (error: any) {
      console.log();

  }

}
async opbkkaccnotnull(info){
  try {
    let rs: any = await this.opbkkService.getopbkkaccnotnull(info);
    // console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.all_notnullcase = rs.results.rows[0].all_notnullcase;
      this.debit_notnull = rs.results.rows[0].debit_notnull;
      this.recieve = rs.results.rows[0].recieve;
      this.sum_diff = rs.results.rows[0].sum_diff;
      this.diffgain = rs.results.rows[0].diffgain;
      this.diffloss = rs.results.rows[0].diffloss;

    }
  } catch (error: any) {
      console.log();
  }

}

async opbkkall(){
      // this.opbkkcall = +this.all_notnullcase + +this.opbkkaccnull_count;
      this.opbkksumdebit = +this.debit_notnull + +this.opbkkaccnull_sum;
}

async tab_opbkknull(info){
  this.itemopbkkNull= [];
  this.itemsExeclopbkkNull=[];
  try {
      let rs: any = await this.opbkkService.getopbkknull(info);
      let data:any = rs.results.rows;
      let xx:string = data
      this.countRowstab1 = xx.length
      // console.log(rs);
      if (rs.results.ok){
        this.itemsExeclopbkkNull = data;
        this.itemopbkkNull = new MatTableDataSource<_opbkknull>(data);
        this.ngAfterViewInit_opbkknull();

      }
    } catch (error: any) {
        console.log();
    }

  }

  async tab_opbkknotnull(info){
      this.itemopbkknotNull= [];
      this.itemsExeclopbkknotNull=[];
      try {
          let rs: any = await this.opbkkService.getopbkknotnull(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab2 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclopbkknotNull = data;
            this.itemopbkknotNull = new MatTableDataSource<_opbkknotnull>(data);
            this.ngAfterViewInit_opbkknotnull();

          }
        } catch (error: any) {
            console.log();
        }
  }

  async tab_opbkkaccbydate(info){
      this.itemopbkkaccbydate= [];
      this.itemsExeclopbkkaccbydate=[];
      try {
          let rs: any = await this.opbkkService.getopbkkaccbydate(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab3 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclopbkkaccbydate = data;
            this.itemopbkkaccbydate = new MatTableDataSource<_opbkkaccbydate>(data);
            this.ngAfterViewInit_opbkkaccbydate();

          }
        } catch (error: any) {
            console.log();
        }

  }
  async tab_opbkkbyerror(info){
    this.itemerrorcode= [];
    this.itemerrorcodeExecl=[];
    try {
        let rs: any = await this.opbkkService.gettoperrorcode(info);
        let data:any = rs.results.rows;
        let xx:string = data
        this.countRowstab4 = xx.length
        // console.log(rs);
        if (rs.results.ok){
          this.itemerrorcodeExecl = data;
          this.itemerrorcode = new MatTableDataSource<_errorcode>(data);
          this.ngAfterViewInit_opbkkerrorcode();

        }
      } catch (error: any) {
          console.log();
      }

}
      exportAsXLSXNull():void {
          this.excelService.exportAsExcelFile(this.itemsExeclopbkkNull, 'ExporttoExcel');
        }

      exportAsXLSXnotNull():void {
       this.excelService.exportAsExcelFile(this.itemsExeclopbkknotNull, 'ExporttoExcel');
     }

     exportAsXLSXAccbyDate():void {
      this.excelService.exportAsExcelFile(this.itemsExeclopbkkaccbydate, 'ExporttoExcel');
    }

    exportAsXLSXErrorcode():void {
      this.excelService.exportAsExcelFile(this.itemerrorcode, 'ExporttoExcel');
    }
    //get debt_account
    onOptionsSelected(){
      this.isCheckbox  = false;
      console.log('ACC',this.selecteditemaccount);
      
    }
    logCheckbox() {
      if(this.isCheckbox === true) {
        this.selecteditemaccount = '';
      }
      console.log(this.isCheckbox);
    }

    _setDataSource(indexNumber) {
      setTimeout(() => {
        switch (indexNumber) {
          case 0:
            !this.itemopbkkNull.paginator ? this.itemopbkkNull.paginator = this.paginator : null;
            break;
          case 1:
            !this.itemopbkknotNull.paginator ? this.itemopbkknotNull.paginator = this.paginator2 : null;
            break;
          case 2:
            !this.itemopbkkaccbydate.paginator ? this.itemopbkkaccbydate.paginator = this.paginator3 : null;
            break;
          case 3:
            !this.itemerrorcode.paginator ? this.itemerrorcode.paginator = this.paginator4 : null;
        }
      });
    }
}