import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';


import { RepOpBkkRoutingModule } from './rep-op-bkk-routing.module';
import { RepOpBkkComponent } from './rep-op-bkk.component';

import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    RepOpBkkComponent
  ],
  providers:[ExcelService],
  imports: [
    RepOpBkkRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class RepOpBkkModule { }
