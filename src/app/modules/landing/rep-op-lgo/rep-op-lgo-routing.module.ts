import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RepOpLgoComponent } from './rep-op-lgo.component'

const routes: Routes = [
  {
    path:'',component:RepOpLgoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepOpLgoRoutingModule { }
