import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepOpLgoComponent } from './rep-op-lgo.component';

describe('RepOpLgoComponent', () => {
  let component: RepOpLgoComponent;
  let fixture: ComponentFixture<RepOpLgoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepOpLgoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepOpLgoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
