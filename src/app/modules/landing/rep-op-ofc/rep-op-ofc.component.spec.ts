import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepOpOfcComponent } from './rep-op-ofc.component';

describe('RepOpOfcComponent', () => {
  let component: RepOpOfcComponent;
  let fixture: ComponentFixture<RepOpOfcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepOpOfcComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepOpOfcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
