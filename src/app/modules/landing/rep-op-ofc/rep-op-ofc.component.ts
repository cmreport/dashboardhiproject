import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator} from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { OpofcService } from '../../../services/opofc.service'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';

import { ChartType } from '../../landing/rep-op-ofc/rep-op-ofc.model'

export interface _opofcnull {

  hn : any;
  vn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  visit_date: any;
  visit_time: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  error_code: any;
  error_name: any;
  total_summary: any;
  projcode: any;
}
export interface _opofcnotnull {
  hn : any;
  vn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  visit_date: any;
  visit_time: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  error_code: any;
  error_name: any;
  total_summary: any;
  projcode: any;
}

export interface _opofcaccbydate {
  visit_date: any;
  allcase: any;
  debit: any;
  nullcase: any;
  nulldebit: any;
  notnullcase: any;
  notnulldebit: any;
  recieve: any;
  diff: any;
}

export interface _errorcode{
  error_code:any;
  error_name:any;
  counterrorcode:any;
}

@Component({
  selector: 'app-rep-op-ofc',
  templateUrl: './rep-op-ofc.component.html',
  styleUrls: ['./rep-op-ofc.component.scss']
})
export class RepOpOfcComponent {

  //count datatable
countRowstab1 :any;
countRowstab2 :any;
countRowstab3 :any;
countRowstab4 :any;
//สร้างตัวแปร items,datestart,dateend
//itemerrorcode
itemerrorcode:any=[];
itemerrorcodeExecl:any=[];

//opofcaccbydate
itemopofcaccbydate : any =[];
itemsExeclopofcaccbydate:any = [];

//opofcnotNull
itemopofcnotNull : any = [];
itemsExeclopofcnotNull:any = [];

//opofcNull
itemopofcNull : any = [];
itemsExeclopofcNull:any = [];

//accnull
opofcaccnull_count : any=0;
opofcaccnull_sum : any=0;

 //debt_account
 itemaccount:any = [];
 selecteditemaccount:any='';

//accnotnull
all_notnullcase: any=0;
debit_notnull: any=0;
recieve: any=0;
sum_diff: any=0;
diffgain: any=0;
diffloss: any=0;
opofccall : any =0;
opofcsumdebit : any =0;

startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
isCheckbox : boolean = false;

// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
opofcnulldisplayedColumns: string[] = ['vn','hn','cid','fullname',
'acc_name', 'visit_date','visit_time','charge','paid','debt','repno','projcode','error_code','error_name','total_summary'];

opofcnotnulldisplayedColumns: string[] = ['vn','hn','cid','fullname',
'acc_name','visit_date', 'visit_time','charge','paid','debt','repno','projcode','error_code','error_name',
'total_summary'];

opofcaccbydatedisplayedColumns: string[] = ['visit_date','allcase','debit','nullcase',
'nulldebit', 'notnullcase', 'notnulldebit','recieve','diff'];

errorcodedisplayedColumns: string[] = ['error_code','error_name','counterror'];


@ViewChild('paginator') paginator: MatPaginator;
@ViewChild('paginator2') paginator2: MatPaginator;
@ViewChild('paginator3') paginator3: MatPaginator;
@ViewChild('paginator4') paginator4: MatPaginator;

ngAfterViewInit_opofcnull() {
    this.itemopofcNull.paginator = this.paginator;
  }
  ngAfterViewInit_opofcnotnull() {
    this.itemopofcnotNull.paginator = this.paginator2;
  }
  ngAfterViewInit_opofcaccbydate() {
    this.itemopofcaccbydate.paginator = this.paginator3;
  }
 
  ngAfterViewInit_opofcerrorcode() {
    this.itemerrorcode.paginator = this.paginator4;
  }

  constructor(
    private router: Router,
    private opofcService:OpofcService,
    private excelService : ExcelService
) {} 

ngOnInit(): void {
  this.isCheckbox  = true;
  this.getLoadData()
  //this.getopofcacc()
}

//get DATA TABLE
async getLoadData(){
    await this.getLookup_debtaccount();
}
//get DATA Table debt_account of db HICM
async getLookup_debtaccount(){
  let lcACC :any = await this.opofcService.getselect_debt_account_hi();
  console.log('lcACC',lcACC);
  
  this.itemaccount = lcACC;
}


async getopofcacc(){

  let lcCode:any = this.selecteditemaccount; //OFC
  let info: any = {
  "accType": lcCode,
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info:',info);
  //ผู้ป่วยส่งเบิกทั้งหมด OK
  await this.opCountofcAll(info)
  //ดำเนินการเสร็จ OK 
  await this.opofcaccnotnull(info);
  //รอดำเนินการ OK 
  await this.opofcaccnull(info);
  //tab 1 ok
  await this.tab_opofcnull(info);
  //tab 2 ok
  await this.tab_opofcnotnull(info);
  //tab 3 ok
  await this.tab_opofcaccbydate(info);
  //tab 4 ok
  await this.tab_opofcbyerror(info);  
}

async opCountofcAll(info){ 
  try {
    let rs: any = await this.opofcService.getstmopCount(info);
    let data:any = rs.results;
    // console.log('xx',rs);
    if (rs.results.ok){
      this.opofccall = rs.results.rows[0].all_op ;

    }
  } catch (error: any) {
      console.log(error);
  }
}
async opofcaccnull(info){
  try {
    let rs: any = await this.opofcService.getopofcaccnull(info);
    // console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.opofcaccnull_count = rs.results.rows[0].count_vn;
      this.opofcaccnull_sum = rs.results.rows[0].sum_debt;
      console.log(this.opofcaccnull_count);

    }
  } catch (error: any) {
      console.log();

  }

}
async opofcaccnotnull(info){
  try {
    let rs: any = await this.opofcService.getopofcaccnotnull(info);
    // console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.all_notnullcase = rs.results.rows[0].all_notnullcase;
      this.debit_notnull = rs.results.rows[0].debit_notnull;
      this.recieve = rs.results.rows[0].recieve;
      this.sum_diff = rs.results.rows[0].sum_diff;
      this.diffgain = rs.results.rows[0].diffgain;
      this.diffloss = rs.results.rows[0].diffloss;

    }
  } catch (error: any) {
      console.log();
  }

}

async opofcall(){
      // this.opofccall = +this.all_notnullcase + +this.opofcaccnull_count;
      this.opofcsumdebit = +this.debit_notnull + +this.opofcaccnull_sum;
}

async tab_opofcnull(info){
  this.itemopofcNull= [];
  this.itemsExeclopofcNull=[];
  try {
      let rs: any = await this.opofcService.getopofcnull(info);
      let data:any = rs.results.rows;
      let xx:string = data
      this.countRowstab1 = xx.length
      // console.log(rs);
      if (rs.results.ok){
        this.itemsExeclopofcNull = data;
        this.itemopofcNull =  new MatTableDataSource<_opofcnull>(data);
        this.ngAfterViewInit_opofcnull();

      }
    } catch (error: any) {
        console.log();
    }

  }

  async tab_opofcnotnull(info){
      this.itemopofcnotNull= [];
      this.itemsExeclopofcnotNull=[];
      try {
          let rs: any = await this.opofcService.getopofcnotnull(info);
          let data:any = rs.results.rows;
          let xx:string =  data
          this.countRowstab2 = xx.length
          // console.log(rs);
          if (rs.results.ok){           
            this.itemsExeclopofcnotNull = data;
            this.itemopofcnotNull = new MatTableDataSource<_opofcnotnull>(data);
            this.ngAfterViewInit_opofcnotnull();
           
          }
        } catch (error: any) {
            console.log();
        }
  }

  async tab_opofcaccbydate(info){
      this.itemopofcaccbydate= [];
      this.itemsExeclopofcaccbydate=[];
      try {
          let rs: any = await this.opofcService.getopofcaccbydate(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab3 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclopofcaccbydate = data;
            this.itemopofcaccbydate = new MatTableDataSource<_opofcaccbydate>(data);
            this.ngAfterViewInit_opofcaccbydate();

          }
        } catch (error: any) {
            console.log();
        }

  }
  async tab_opofcbyerror(info){
    this.itemerrorcode= [];
    this.itemerrorcodeExecl=[];
    try {
        let rs: any = await this.opofcService.gettoperrorcode(info);
        let data:any = rs.results.rows;
        let xx:string = data
        this.countRowstab4 = xx.length
        // console.log(rs);
        if (rs.results.ok){
          this.itemerrorcodeExecl = data;
          this.itemerrorcode = new MatTableDataSource<_errorcode>(data);
          this.ngAfterViewInit_opofcerrorcode();

        }
      } catch (error: any) {
          console.log();
      }

}

      exportAsXLSXNull():void {
          this.excelService.exportAsExcelFile(this.itemsExeclopofcNull, 'ExporttoExcel');
        }

      exportAsXLSXnotNull():void {
       this.excelService.exportAsExcelFile(this.itemsExeclopofcnotNull, 'ExporttoExcel');
     }

     exportAsXLSXAccbyDate():void {
      this.excelService.exportAsExcelFile(this.itemsExeclopofcaccbydate, 'ExporttoExcel');
    }
    exportAsXLSXErrorcode():void {
      this.excelService.exportAsExcelFile(this.itemerrorcodeExecl, 'ExporttoExcel');
    }
    //get debt_account
    onOptionsSelected(){
      this.isCheckbox  = false;
      console.log('ACC',this.selecteditemaccount);
      
    }
    
    logCheckbox() {
      if(this.isCheckbox === true) {
        this.selecteditemaccount = '';
      }
      console.log(this.isCheckbox);
    }

    _setDataSource(indexNumber) {
      setTimeout(() => {
        switch (indexNumber) {
          case 0:
            !this.itemopofcNull.paginator ? this.itemopofcNull.paginator = this.paginator : null;
            break;
          case 1:
            !this.itemopofcnotNull.paginator ? this.itemopofcnotNull.paginator = this.paginator2 : null;
            break;
          case 2:
            !this.itemopofcaccbydate.paginator ? this.itemopofcaccbydate.paginator = this.paginator3 : null;
            break;
          case 3:
            !this.itemerrorcode.paginator ? this.itemerrorcode.paginator = this.paginator4 : null;
        }
      });
    }
}
