import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { RepOpOfcRoutingModule } from './rep-op-ofc-routing.module';
import { RepOpOfcComponent } from './rep-op-ofc.component';

import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    RepOpOfcComponent
  ],
  providers:[ExcelService],
  imports: [
    RepOpOfcRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class RepOpOfcModule { }
