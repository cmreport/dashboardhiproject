import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RepOpStpComponent} from './rep-op-stp.component';
const routes: Routes = [
  {
    path:'',component:RepOpStpComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepOpStpRoutingModule { }
