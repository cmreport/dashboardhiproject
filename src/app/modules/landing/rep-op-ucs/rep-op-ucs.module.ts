import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { RepOpUcsRoutingModule } from './rep-op-ucs-routing.module';
import { RepOpUcsComponent } from './rep-op-ucs.component';

import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    RepOpUcsComponent
  ],
  providers:[ExcelService],
  imports: [
    RepOpUcsRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class RepOpUcsModule { }
