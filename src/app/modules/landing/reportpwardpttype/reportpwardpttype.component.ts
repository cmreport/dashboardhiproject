import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ReportpwardpttypeService } from 'app/services/reportpwardpttype.service';
import * as moment from 'moment-timezone';
import { AlertService } from 'app/services/alert.service';
import { ExcelService } from 'app/services/excel.service';

export interface PeriodicElement {

    name: string;
    t_icu: string;
    t_ci: string;
    t_mi: string;
    t_cl: string;
    t_si: string;
  }

@Component({
  selector: 'app-reportpwardpttype',
  templateUrl: './reportpwardpttype.component.html',
  styleUrls: ['./reportpwardpttype.component.scss']
})
export class ReportpwardpttypeComponent {

datestart: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
dateend: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
//สร้างตัวแปร items,datestart,dateend
items: any = [];
itemsExecl:any = [];
txtcodewd: any;
txtnamecodewd: any;
txtyear: any;
// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
displayedColumns: string[] = ['name','t_icu','t_ci','t_mi','t_cl',
't_si'];
//ตัวแปรรับค่า Accesstype
accesstype: any;
//   dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

@ViewChild(MatPaginator) paginator: MatPaginator;

ngAfterViewInit(){
 //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
 //Add 'implements AfterViewInit' to the class.
 this.items.paginator = this.paginator;
}

constructor(
 private router: Router,
 private reportpwardpttypeService: ReportpwardpttypeService,
 private alertService: AlertService,
 private excelService:ExcelService

){}

ngOnInit(): void {
 //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
 //Add 'implements OnInit' to the class.
 this.getInfo();

}

async getInfo(){
     //กำหนด Array ให้ว่างก่อนเพื่อรอรับค่า Data
     this.items= [];
     this.itemsExecl=[];

     //ส่งค่าตัวแปร datestart,dateend เป็น Object

     let info: any = {"datestart":moment(this.datestart).tz('Asia/Bangkok').format('YYYY-MM-DD'),
     "dateend":moment(this.dateend).tz('Asia/Bangkok').format('YYYY-MM-DD')}
     console.log(info);

     try {
         let rs: any = await this.reportpwardpttypeService.selete_pd_pttype(info);
         console.log(rs);
         if (rs.length > 0){
            this.itemsExecl=rs;
            this.items = new MatTableDataSource<PeriodicElement>(rs);
            this.ngAfterViewInit();
         }
     } catch (error: any) {
         console.log();

     }
 }

async getInfoClick(){
     //กำหนด Array ให้ว่างก่อนเพื่อรอรับค่า Data
     this.items= [];
     this.itemsExecl=[];
     //ส่งค่าตัวแปร datestart,dateend เป็น Object
     let info: any = {"datestart":moment(this.datestart).tz('Asia/Bangkok').format('YYYY-MM-DD'),
     "dateend":moment(this.dateend).tz('Asia/Bangkok').format('YYYY-MM-DD')}
     console.log(info);

     try {
         let rs: any = await this.reportpwardpttypeService.selete_pd_pttype(info);
         console.log(rs);
         if (rs.length > 0){
            this.itemsExecl=rs;
            this.items = new MatTableDataSource<PeriodicElement>(rs);
            this.ngAfterViewInit();

         }else{
             this.alertService.info('ไม่พบข้อมูล....!!!')
         }
     } catch (error: any) {
         console.log();

     }
 }

 exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.itemsExecl, 'ExporttoExcel');
  }

}

