import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportpwardpttypeComponent } from './reportpwardpttype.component';
import { Route,RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ExcelService } from 'app/services/excel.service';

const reportpwardpttypeRouts:Route[]=[
    {
        path :'',
        component:ReportpwardpttypeComponent
    }
]

@NgModule({
  declarations: [
    ReportpwardpttypeComponent
  ],
  providers:[ExcelService],
  imports: [
    CommonModule,
    RouterModule.forChild(reportpwardpttypeRouts),
    SharedModule
  ]
})
export class ReportpwardpttypeModule { }
