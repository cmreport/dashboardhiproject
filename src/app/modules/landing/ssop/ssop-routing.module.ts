import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SsopComponent } from './ssop.component'
const routes: Routes = [
  {
    path:'',component:SsopComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SsopRoutingModule { }
