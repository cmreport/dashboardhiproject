import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/ssop/ssop.model';

import { SsopService } from '../../../services/ssop.service'


export interface _ssopstatusFlgA {
  invno: any;
  dttran_date: any;
  dttran_time: any;
  hcode: any;
  hn: any;
  pid: any;
  name: any;
  hmain: any;
  amount: any;
  paid: any;
  claimamt: any;
  tflag: any;

}

export interface _ssopstatusFlgE {
  invno: any;
  dttran_date: any;
  dttran_time: any;
  hcode: any;
  hn: any;
  pid: any;
  name: any;
  hmain: any;
  amount: any;
  paid: any;
  claimamt: any;
  tflag: any;

}

export interface _ssopstatusFlgD {
  invno: any;
  dttran_date: any;
  dttran_time: any;
  hcode: any;
  hn: any;
  pid: any;
  name: any;
  hmain: any;
  amount: any;
  paid: any;
  claimamt: any;
  tflag: any;

}

export interface _ssopstatusFlgN {
  invno: any;
  dttran_date: any;
  dttran_time: any;
  hcode: any;
  hn: any;
  pid: any;
  name: any;
  hmain: any;
  amount: any;
  paid: any;
  claimamt: any;
  tflag: any;

}


@Component({
  selector: 'app-ssop',
  templateUrl: './ssop.component.html',
  styleUrls: ['./ssop.component.scss']
})
export class SsopComponent {

   //สร้างตัวแปร items,datestart,dateend
//item Ssop statusflg N
itemSsopstatusflgN : any =[];
itemsExeclitemSsopstatusflgN:any = []; 

//item Ssop statusflg A
itemSsopstatusflgA : any =[];
itemsExeclitemSsopstatusflgA:any = []; 

//item Ssop statusflg E
itemSsopstatusflgE : any =[];
itemsExeclitemSsopstatusflgE:any = []; 

//item Ssop statusflg D
itemSsopstatusflgD : any =[];
itemsExeclitemSsopstatusflgD:any = []; 

//get Value Count status
ssop_all_status: any=0;
ssop_statflg_A: any=0;
ssop_statflg_E: any=0;
ssop_statflg_D: any=0;
ssop_statflg_N: any=0;

itemCln:any = [];
selecteditemCln:any='';

startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');

// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
ssopstatusflgAdisplayedColumns: string[] = ['invno','dttran_date','dttran_time','hcode','hn','pid','name','hmain',
  'amount','paid','claimamt','tflag'];
  
ssopstatusflgNdisplayedColumns: string[] = ['invno','dttran_date','dttran_time','hcode','hn','pid','name','hmain',
  'amount','paid','claimamt','tflag'];
  
ssopstatusflgEdisplayedColumns: string[] = ['invno','dttran_date','dttran_time','hcode','hn','pid','name','hmain',
  'amount','paid','claimamt','tflag'];
  
ssopstatusflgDdisplayedColumns: string[] = ['invno','dttran_date','dttran_time','hcode','hn','pid','name','hmain',
  'amount','paid','claimamt','tflag'];
  

@ViewChild(MatPaginator) paginator: MatPaginator;
@ViewChild('paginator2') paginator2: MatPaginator;
@ViewChild('paginator3') paginator3: MatPaginator;
@ViewChild('paginator4') paginator4: MatPaginator;
  ngAfterViewInit_ssopstatusflgA() {
      this.itemSsopstatusflgA.paginator = this.paginator;
    }
    ngAfterViewInit_ssopstatusflgC() {
      this.itemSsopstatusflgE.paginator = this.paginator2;
    }
    ngAfterViewInit_ssopstatusflgD() {
      this.itemSsopstatusflgD.paginator = this.paginator3;
    }
  
    ngAfterViewInit_ssopstatusflgN() {
      this.itemSsopstatusflgN.paginator = this.paginator4;
    }
    constructor(
      private router: Router,
      private ssopService:SsopService,
      private excelService : ExcelService
  ) {} 
  
  ngOnInit():void{
    this.getLookupDATA();
  }

//get DATA Table
async getLookupDATA(){
  await this.getLookupCLN()
 }
//get DATA Table Cln of dbHI
async getLookupCLN(){
  let lcCln:any = await this.ssopService.select_cln();
  this.itemCln = lcCln;
 }

 async getSsopInfo(){
  let lcCode:any = this.selecteditemCln; //cln
  let info: any = {
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info',info);
  
  //แสดงจำนวนผู้ป่วย SSOP
  await this.getssopallStatusflg(info);
  await this.tab_ssopstatusflgA(info);
  await this.tab_ssopstatusflgE(info);
  await this.tab_ssopstatusflgD(info);
  await this.tab_ssopstatusflgN(info);

} 



async getssopallStatusflg(info){
  try{
    let rs:any = await this.ssopService.getssop_all_status(info);
    let data:any = rs[0];
    console.log(rs);
    if(rs.length > 0){
      this.ssop_all_status =data.countall;
      this.ssop_statflg_A = data.status_a;
      this.ssop_statflg_E = data.status_e;
      this.ssop_statflg_D = data.status_d;
      this.ssop_statflg_N = data.status_null;
    }    
  }catch(error:any){
    console.log();    
  }
}

async tab_ssopstatusflgA(info){
  this.itemSsopstatusflgA= [];
  this.itemsExeclitemSsopstatusflgA=[];
  try {
      let rs: any = await this.ssopService.getssop_statusflgA(info);
      let data:any = rs.results.rows;
      console.log('A',rs);
      if (rs.results.ok){
        this.itemsExeclitemSsopstatusflgA = data;
        this.itemSsopstatusflgA = new MatTableDataSource<_ssopstatusFlgA>(data);
        this.ngAfterViewInit_ssopstatusflgA();

      }
    } catch (error: any) {
        console.log();
    }

  }
async tab_ssopstatusflgE(info){
  this.itemSsopstatusflgE= [];
  this.itemsExeclitemSsopstatusflgE=[];
  try {
      let rs: any = await this.ssopService.getssop_statusflgE(info);
      let data:any = rs.results.rows;
      console.log('E',rs);
      if (rs.results.ok){
        this.itemsExeclitemSsopstatusflgE = data;
        this.itemSsopstatusflgE = new MatTableDataSource<_ssopstatusFlgE>(data);
        this.ngAfterViewInit_ssopstatusflgC();

      }
    } catch (error: any) {
        console.log();
    }

  }

  async tab_ssopstatusflgD(info){
    this.itemSsopstatusflgD= [];
    this.itemsExeclitemSsopstatusflgD=[];
    try {
        let rs: any = await this.ssopService.getssop_statusflgD(info);
        let data:any = rs.results.rows;
        console.log('D',rs);
        if (rs.results.ok){
          this.itemsExeclitemSsopstatusflgD = data;
          this.itemSsopstatusflgD = new MatTableDataSource<_ssopstatusFlgD>(data);
          this.ngAfterViewInit_ssopstatusflgD();
  
        }
      } catch (error: any) {
          console.log();
      }
  
    }
    async tab_ssopstatusflgN(info){
      this.itemSsopstatusflgN= [];
      this.itemsExeclitemSsopstatusflgN=[];
      try {
          let rs: any = await this.ssopService.getssop_statusflgN(info);
          let data:any = rs.results.rows;
          console.log('N',rs);
          if (rs.results.ok){
            this.itemsExeclitemSsopstatusflgN = data;
            this.itemSsopstatusflgN = new MatTableDataSource<_ssopstatusFlgN>(data);
            this.ngAfterViewInit_ssopstatusflgN();
    
          }
        } catch (error: any) {
            console.log();
        }
    
      } 
 //get IDPM
onOptionsSelected(){
  console.log('cln',this.selecteditemCln);
  
 }  

exportAsXLSXFlagN():void {
  this.excelService.exportAsExcelFile(this.itemsExeclitemSsopstatusflgN, 'ExporttoExcel');
}

exportAsXLSXFlagA():void {
this.excelService.exportAsExcelFile(this.itemsExeclitemSsopstatusflgA, 'ExporttoExcel');
}

exportAsXLSXFlagE():void {
this.excelService.exportAsExcelFile(this.itemsExeclitemSsopstatusflgE, 'ExporttoExcel');
}
exportAsXLSXFlagD():void {
this.excelService.exportAsExcelFile(this.itemsExeclitemSsopstatusflgD, 'ExporttoExcel');
}
 _setDataSource(indexNumber) {
  setTimeout(() => {
    switch (indexNumber) {
      case 0:
        !this.itemSsopstatusflgA.paginator ? this.itemSsopstatusflgA.paginator = this.paginator : null;
        break;
      case 1:
        !this.itemSsopstatusflgE.paginator ? this.itemSsopstatusflgE.paginator = this.paginator2 : null;
        break;
      case 2:
        !this.itemSsopstatusflgD.paginator ? this.itemSsopstatusflgD.paginator = this.paginator3 : null;
        break;
      case 3:
        !this.itemSsopstatusflgN.paginator ? this.itemSsopstatusflgN.paginator = this.paginator4 : null;
    }
  });
}
}
