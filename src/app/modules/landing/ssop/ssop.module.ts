import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { SsopRoutingModule } from './ssop-routing.module';
import { SsopComponent } from './ssop.component';

import { ExcelService } from 'app/services/excel.service';
@NgModule({
  declarations: [
    SsopComponent
  ],
  providers:[ExcelService],
  imports: [
    SsopRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class SsopModule { }
