import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { StmIpBkkRoutingModule } from './stm-ip-bkk-routing.module';
import { StmIpBkkComponent } from './stm-ip-bkk.component';
import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    StmIpBkkComponent
  ],
  providers:[ExcelService],
  imports: [
    StmIpBkkRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class StmIpBkkModule { }
