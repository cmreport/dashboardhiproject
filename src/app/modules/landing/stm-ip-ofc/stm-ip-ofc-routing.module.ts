import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StmIpOfcComponent} from '../stm-ip-ofc/stm-ip-ofc.component';
const routes: Routes = [
  {
    path:'',component:StmIpOfcComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StmIpOfcRoutingModule { }
