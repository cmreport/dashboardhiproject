import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StmIpOfcComponent } from './stm-ip-ofc.component';

describe('StmIpOfcComponent', () => {
  let component: StmIpOfcComponent;
  let fixture: ComponentFixture<StmIpOfcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StmIpOfcComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StmIpOfcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
