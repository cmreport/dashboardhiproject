import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { IpofcService } from '../../../services/ipofc.service'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';

import { ChartType } from '../../landing/stm-ip-ofc/stm-ip-ofc.model'

export interface _stpipofc {
  stm_no:any;
  repno: any;
  no: any;
  hn: any;
  an: any;
  cid: any;
  fullname: any;
  date_serv: any;
  date_dch: any;
  projcode: any;
  adjrw: any;
  charge: any;
  insure: any;
  in_room: any;
  in_prcd: any;
  in_meditem: any;
  in_dx: any;
  in_carfare: any;
  in_case: any;
  in_etc: any;
  total_summary: any;
  diff:any;
}


@Component({
  selector: 'app-stm-ip-ofc',
  templateUrl: './stm-ip-ofc.component.html',
  styleUrls: ['./stm-ip-ofc.component.scss']
})

export class StmIpOfcComponent {
//stm ipofc
itemstmipofc : any =[];
itemsExeclStmipofc:any = [];

all_case: any;
debt: any;
receive: any;
sum_diff: any;
diffgain: any;
diffloss: any;
repno:any;

// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
stmipofcdisplayedColumns: string[] = ['stm_no','repno','no','hn','an',
'cid', 'fullname', 'date_serv','date_dch','projcode','adjrw','charge','insure',
'in_room','in_prcd','in_meditem','in_dx','in_carfare','in_case','in_etc','total_summary','diff'];

@ViewChild(MatPaginator) paginator: MatPaginator;
ngAfterViewInit_stmipofc() {
    this.itemstmipofc.paginator = this.paginator;
  }
  constructor(
    private router: Router,
    private ipofcService: IpofcService,
    private alertService: AlertService,
    private excelService:ExcelService
   
   ){}

   ngOnInit(): void {
    this.getStmipOfc();
  }

  async getStmipOfc(){
    let info: any = {
      "stm_no": this.repno,
      }
      console.log(this.repno);
  
      await this.stmipofc_stmno(info);
      await this.stmipofc_sum_stmno(info);
    // let info: any = {
    // "repno": this.repno,
    // }
    // console.log(this.repno);

    // await this.stmipofc(info);
    // await this.stmipofc_sum(info);
  }

 async stmipofc(info){
    this.itemstmipofc= [];
    this.itemsExeclStmipofc=[];
    try {
        let rs: any = await this.ipofcService.getstmipofc(info);
        let data:any = rs.results.rows;
        console.log(rs);
        if (rs.results.ok){
          this.itemsExeclStmipofc = data;
          this.itemstmipofc = new MatTableDataSource<_stpipofc>(data);
          this.ngAfterViewInit_stmipofc();

        }
      } catch (error: any) {
          console.log();
      }
 }

 async stmipofc_stmno(info){
  this.itemstmipofc= [];
  this.itemsExeclStmipofc=[];
  try {
      let rs: any = await this.ipofcService.getstmipofc_stmno(info);
      let data:any = rs.results.rows;
      console.log(rs);
      if (rs.results.ok){
        this.itemsExeclStmipofc = data;
        this.itemstmipofc = new MatTableDataSource<_stpipofc>(data);
        this.ngAfterViewInit_stmipofc();

      }
    } catch (error: any) {
        console.log();
    }
}

 async stmipofc_sum(info){
    try {
      let rs: any = await this.ipofcService.getstmipofc_sum(info);
      console.log(rs);
      if (rs.results.ok){
        this.all_case = rs.results.rows[0].all_case;
        console.log('all_case',this.all_case);
        
        this.debt = rs.results.rows[0].debt;
        this.receive = rs.results.rows[0].receive;
        this.sum_diff = rs.results.rows[0].sum_diff;
        this.diffgain = rs.results.rows[0].diffgain;
        this.diffloss = rs.results.rows[0].diffloss;

      }
    } catch (error: any) {
        console.log();
    }

  }

async stmipofc_sum_stmno(info){
    try {
      let rs: any = await this.ipofcService.getstmipofc_sum_stmno(info);
      console.log(rs);
      if (rs.results.ok){
        this.all_case = rs.results.rows[0].all_case;
        console.log('all_case',this.all_case);
        
        this.debt = rs.results.rows[0].debt;
        this.receive = rs.results.rows[0].receive;
        this.sum_diff = rs.results.rows[0].sum_diff;
        this.diffgain = rs.results.rows[0].diffgain;
        this.diffloss = rs.results.rows[0].diffloss;

      }
    } catch (error: any) {
        console.log();
    }

  }  
 exportAsXLSXStmipOfc():void {
    this.excelService.exportAsExcelFile(this.itemsExeclStmipofc, 'ExporttoExcel');
  }

}