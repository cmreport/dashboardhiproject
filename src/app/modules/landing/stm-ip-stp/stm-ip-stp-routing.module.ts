import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StmIpStpComponent} from '../stm-ip-stp/stm-ip-stp.component';

const routes: Routes = [
  {
    path:'',component:StmIpStpComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StmIpStpRoutingModule { }
