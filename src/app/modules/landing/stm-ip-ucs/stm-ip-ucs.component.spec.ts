import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StmIpUcsComponent } from './stm-ip-ucs.component';

describe('StmIpUcsComponent', () => {
  let component: StmIpUcsComponent;
  let fixture: ComponentFixture<StmIpUcsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StmIpUcsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StmIpUcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
