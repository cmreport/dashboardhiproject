import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ChartType } from '../stm-ip-ofc/stm-ip-ofc.model'
import { ExcelService } from 'app/services/excel.service';

import {IpsssService } from '../../../services/ipsss.service'

export interface _stpipsss {
  stm_no:any;
  repno: any;
  no: any;
  hn: any;
  an: any;
  cid: any;
  fullname: any;
  date_serv: any;
  date_dch: any;
  projcode: any;
  adjrw: any;
  charge: any;
  insure: any;
  in_room: any;
  in_prcd: any;
  in_meditem: any;
  in_dx: any;
  in_carfare: any;
  in_case: any;
  in_etc: any;
  total_summary: any;
  diff:any;
}

@Component({
  selector: 'app-stm-ipop-sss',
  templateUrl: './stm-ipop-sss.component.html',
  styleUrls: ['./stm-ipop-sss.component.scss']
})
export class StmIpSssComponent {
//stm ipsss
itemstmipsss : any =[];
itemsExeclStmipsss:any = [];

all_case: any;
debt: any;
receive: any;
sum_diff: any;
diffgain: any;
diffloss: any;
repno:any;

// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
stmipsssdisplayedColumns: string[] = ['stm_no','repno','no','hn','an',
'cid', 'fullname', 'date_serv','date_dch','projcode','adjrw','charge','insure',
'in_room','in_prcd','in_meditem','in_dx','in_carfare','in_case','in_etc','total_summary','diff'];

@ViewChild(MatPaginator) paginator: MatPaginator;
ngAfterViewInit_stmipsss() {
    this.itemstmipsss.paginator = this.paginator;
  }

  constructor(
    private router: Router,
    private ipsssService: IpsssService,
    private alertService: AlertService,
    private excelService:ExcelService
   
   ){}

   ngOnInit(): void {
    this.getStmipSss();
  }

  async getStmipSss(){
    let info: any = {
      "stm_no": this.repno,
      }
      console.log(this.repno);
  
      await this.stmipsss_stmno(info);
      await this.stmipsss_sum_stmno(info);
    // let info: any = {
    // "repno": this.repno,
    // }
    // console.log(this.repno);

    // await this.stmipsss(info);
    // await this.stmipsss_sum(info);
  }

 async stmipsss(info){
    this.itemstmipsss= [];
    this.itemsExeclStmipsss=[];
    try {
        let rs: any = await this.ipsssService.getstmipsss(info);
        let data:any = rs.results.rows;
        console.log(rs);
        if (rs.results.ok){
          this.itemsExeclStmipsss = data;
          this.itemstmipsss = new MatTableDataSource<_stpipsss>(data);
          this.ngAfterViewInit_stmipsss();

        }
      } catch (error: any) {
          console.log();
      }
 }
 async stmipsss_stmno(info){
  this.itemstmipsss= [];
  this.itemsExeclStmipsss=[];
  try {
      let rs: any = await this.ipsssService.getstmipsss_stmno(info);
      let data:any = rs.results.rows;
      console.log(rs);
      if (rs.results.ok){
        this.itemsExeclStmipsss = data;
        this.itemstmipsss = new MatTableDataSource<_stpipsss>(data);
        this.ngAfterViewInit_stmipsss();

      }
    } catch (error: any) {
        console.log();
    }
}
 async stmipsss_sum(info){
    try {
      let rs: any = await this.ipsssService.getstmipsss_sum(info);
      console.log(rs);
      if (rs.results.ok){
        this.all_case = rs.results.rows[0].all_case;
        console.log('all_case',this.all_case);
        
        this.debt = rs.results.rows[0].debt;
        this.receive = rs.results.rows[0].receive;
        this.sum_diff = rs.results.rows[0].sum_diff;
        this.diffgain = rs.results.rows[0].diffgain;
        this.diffloss = rs.results.rows[0].diffloss;

      }
    } catch (error: any) {
        console.log();
    }

  }

  async stmipsss_sum_stmno(info){
    try {
      let rs: any = await this.ipsssService.getstmipsss_sum_stmno(info);
      console.log(rs);
      if (rs.results.ok){
        this.all_case = rs.results.rows[0].all_case;
        console.log('all_case',this.all_case);
        
        this.debt = rs.results.rows[0].debt;
        this.receive = rs.results.rows[0].receive;
        this.sum_diff = rs.results.rows[0].sum_diff;
        this.diffgain = rs.results.rows[0].diffgain;
        this.diffloss = rs.results.rows[0].diffloss;

      }
    } catch (error: any) {
        console.log();
    }

  }

 exportAsXLSXStmipSss():void {
    this.excelService.exportAsExcelFile(this.itemsExeclStmipsss, 'ExporttoExcel');
  }

}
