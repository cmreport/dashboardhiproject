import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StmOpBkkComponent} from '../stm-op-bkk/stm-op-bkk.component';
const routes: Routes = [
  {
    path:'',component:StmOpBkkComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StmOpBkkRoutingModule { }
