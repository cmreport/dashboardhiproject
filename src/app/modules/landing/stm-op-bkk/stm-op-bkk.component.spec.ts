import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StmOpBkkComponent } from './stm-op-bkk.component';

describe('StmOpBkkComponent', () => {
  let component: StmOpBkkComponent;
  let fixture: ComponentFixture<StmOpBkkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StmOpBkkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StmOpBkkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
