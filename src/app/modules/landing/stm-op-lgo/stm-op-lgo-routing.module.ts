import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StmOpLgoComponent } from '../stm-op-lgo/stm-op-lgo.component';
const routes: Routes = [
  {
    path:'',component:StmOpLgoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StmOpLgoRoutingModule { }
