import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StmOpOfcComponent} from '../stm-op-ofc/stm-op-ofc.component';

const routes: Routes = [
  {
    path:'',component:StmOpOfcComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StmOpOfcRoutingModule { }
