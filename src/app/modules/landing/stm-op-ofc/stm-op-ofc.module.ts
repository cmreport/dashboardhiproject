import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { StmOpOfcRoutingModule } from './stm-op-ofc-routing.module';
import { StmOpOfcComponent } from './stm-op-ofc.component';
import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    StmOpOfcComponent
  ],
  providers:[ExcelService],
  imports: [
    StmOpOfcRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class StmOpOfcModule { }
