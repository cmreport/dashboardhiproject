import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StmOpStpComponent } from '../stm-op-stp/stm-op-stp.component';

const routes: Routes = [
  {
    path:'',component:StmOpStpComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StmOpStpRoutingModule { }
