import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StmOpStpComponent } from './stm-op-stp.component';

describe('StmOpStpComponent', () => {
  let component: StmOpStpComponent;
  let fixture: ComponentFixture<StmOpStpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StmOpStpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StmOpStpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
