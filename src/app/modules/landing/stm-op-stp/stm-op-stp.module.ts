import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { StmOpStpRoutingModule } from './stm-op-stp-routing.module';
import { StmOpStpComponent } from './stm-op-stp.component';
import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    StmOpStpComponent
  ],
  providers:[ExcelService],
  imports: [
    StmOpStpRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class StmOpStpModule { }
