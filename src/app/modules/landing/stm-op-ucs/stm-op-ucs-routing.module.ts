import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StmOpUcsComponent} from '../stm-op-ucs/stm-op-ucs.component';

const routes: Routes = [
  {
    path:'',component:StmOpUcsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StmOpUcsRoutingModule { }
