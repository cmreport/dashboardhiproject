import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IpDashboardService {

    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }

      async select_idpm() {
        const _url = `${this.apiUrl}/ip-dashboard/select_idpm`;
        return this.httpClient.get(_url, this.httpOptions).toPromise();
     }
     async select_yearbudget() {
      const _url = `${this.apiUrl}/ip-dashboard/select_yearbudget`;
      return this.httpClient.get(_url, this.httpOptions).toPromise();
    }
      async getService_ip_all(info: any) {
          console.log(info);         
          const _url = `${this.apiUrl}/ip-dashboard/service_ip_all`;
          return this.httpClient.post(_url,info,this.httpOptions).toPromise();
        }
      async getService_ip_null(info: any) {
          console.log(info);
          const _url = `${this.apiUrl}/ip-dashboard/service_ip_null`;
          return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      async getService_ip_appro(info: any) {
          console.log(info);
          const _url = `${this.apiUrl}/ip-dashboard/service_ip_appro`;
          return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      async getService_ip_advice(info: any) {
        const _url = `${this.apiUrl}/ip-dashboard/service_ip_advice`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async getService_ip_escape(info: any) {
      const _url = `${this.apiUrl}/ip-dashboard/service_ip_escape`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async getService_ip_refer(info: any) {
        const _url = `${this.apiUrl}/ip-dashboard/service_ip_refer`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async getService_ip_other(info: any) {
      const _url = `${this.apiUrl}/ip-dashboard/service_ip_other`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async getService_ip_nulltype(info: any) {
        const _url = `${this.apiUrl}/ip-dashboard/service_ip_nulltype`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async getService_ip_dead(info: any) {
          const _url = `${this.apiUrl}/ip-dashboard/service_ip_dead`;
          console.log(_url);
          
          return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async test(info: any) {
        const _url = `${this.apiUrl}/ip-dashboard/getIpVisitByPttype`;
        console.log(_url);
        
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
      async getIPsummary(info: any) {
        const _url = `${this.apiUrl}/ip-dashboard/getIPsummary`;       
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    async getreload_referout(){
      window.open(`${window.location.origin}/ip-rfro`,"_blank");
    }
    
    async getreload_referin(){
      window.open(`${window.location.origin}/ip-rfri`,"_blank");
    }

  }

