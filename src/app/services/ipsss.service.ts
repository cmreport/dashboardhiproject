import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class IpsssService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
      getselect_debt_account(){
        const _url = `${this.apiUrl}/hicm-ip-sss/select_debt_account`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getselect_debt_account_hi(){
        const _url = `${this.apiUrl}/hicm-ip-sss/select_debt_account_hi`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getipsssnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-sss/ipsssnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipsssaccnotnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-sss/ipsssaccnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipsssaccnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-sss/ipsssaccnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipsssnotnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-sss/ipsssnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipsssaccbydate(info) {
        const _url = `${this.apiUrl}/hicm-ip-sss/ipsssaccbydate`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipsss(info){
        const _url = `${this.apiUrl}/hicm-ip-sss/stm_ip_sss`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipsss_stmno(info){
        const _url = `${this.apiUrl}/hicm-ip-sss/stm_ip_sss_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipsss_detail(info){
        const _url = `${this.apiUrl}/hicm-ip-sss/stm_ip_sss_detail`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipsss_sum(info){
        const _url = `${this.apiUrl}/hicm-ip-sss/stm_ip_sss_sum`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipsss_sum_stmno(info){
        const _url = `${this.apiUrl}/hicm-ip-sss/stm_ip_sss_sum_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }      
      getstmipCount(info){
        const _url = `${this.apiUrl}/hicm-ip-sss/stm_countan`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      } 
      gettoperrorcode(info){
        const _url = `${this.apiUrl}/hicm-ip-sss/toperrorcode`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }   
      getipsssnotnull_stm(info) {
        const _url = `${this.apiUrl}/hicm-ip-sss/ipsssnotnull_stm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getipsssaccnotnull_stm(info) {
        const _url = `${this.apiUrl}/hicm-ip-sss/ipsssaccnotnull_stm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }        
      
  }
