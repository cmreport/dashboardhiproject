import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class IpstpService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
      getselect_debt_account(){
        const _url = `${this.apiUrl}/hicm-ip-stp/select_debt_account`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getselect_debt_account_hi(){
        const _url = `${this.apiUrl}/hicm-ip-stp/select_debt_account_hi`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getipstpnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-stp/ipstpnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipstpaccnotnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-stp/ipstpaccnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipstpaccnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-stp/ipstpaccnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipstpnotnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-stp/ipstpnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipstpaccbydate(info) {
        const _url = `${this.apiUrl}/hicm-ip-stp/ipstpaccbydate`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipstp(info){
        const _url = `${this.apiUrl}/hicm-ip-stp/stm_ip_stp`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipstp_stmno(info){
        const _url = `${this.apiUrl}/hicm-ip-stp/stm_ip_stp_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipstp_detail(info){
        const _url = `${this.apiUrl}/hicm-ip-stp/stm_ip_stp_detail`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipstp_sum(info){
        const _url = `${this.apiUrl}/hicm-ip-stp/stm_ip_stp_sum`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipstp_sum_stmno(info){
        const _url = `${this.apiUrl}/hicm-ip-stp/stm_ip_stp_sum_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }      
      getstmipCount(info){
        const _url = `${this.apiUrl}/hicm-ip-stp/stm_countan`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      } 
      gettoperrorcode(info){
        const _url = `${this.apiUrl}/hicm-ip-stp/toperrorcode`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      } 
      getipstpnotnull_stm(info) {
        const _url = `${this.apiUrl}/hicm-ip-stp/ipstpnotnull_stm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getipstpaccnotnull_stm(info) {
        const _url = `${this.apiUrl}/hicm-ip-stp/ipstpaccnotnull_stm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }               
  }
