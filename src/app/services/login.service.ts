import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { user } from 'app/mock-api/common/user/data';
@Injectable({
  providedIn: 'root'
})
export class LoginService {


  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    // this.token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }
  //ฟังก์ชั่น Check Login
  login(username: any,password: any){
    //รับค่าแบบ Object 
    let info: any = {"username":username,"password":password}
    //รับค่า route login
    const url: any = `${this.apiUrl}/login`;
    return this.httpClient.post(url,info,this.httpOptions).toPromise();
    
  }

}