import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class OpbkkService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
      getselect_debt_account(){
        const _url = `${this.apiUrl}/hicm-op-bkk/select_debt_account`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getselect_debt_account_hi(){
        const _url = `${this.apiUrl}/hicm-op-bkk/select_debt_account_hi`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getopbkknull(info) {
        const _url = `${this.apiUrl}/hicm-op-bkk/opbkknull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopbkkaccnotnull(info) {
        const _url = `${this.apiUrl}/hicm-op-bkk/opbkkaccnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopbkkaccnull(info) {
        const _url = `${this.apiUrl}/hicm-op-bkk/opbkkaccnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopbkknotnull(info) {
        const _url = `${this.apiUrl}/hicm-op-bkk/opbkknotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopbkkaccbydate(info) {
        const _url = `${this.apiUrl}/hicm-op-bkk/opbkkaccbydate`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopbkk(info){
        const _url = `${this.apiUrl}/hicm-op-bkk/stm_op_bkk`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmopbkk_stmno(info){
        const _url = `${this.apiUrl}/hicm-op-bkk/stm_op_bkk_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmopbkk_detail(info){
        const _url = `${this.apiUrl}/hicm-op-bkk/stm_op_bkk_detail`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopbkk_sum(info){
        const _url = `${this.apiUrl}/hicm-op-bkk/stm_op_bkk_sum`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmopbkk_sum_stmno(info){
        const _url = `${this.apiUrl}/hicm-op-bkk/stm_op_bkk_sum_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmopCount(info){
        const _url = `${this.apiUrl}/hicm-op-bkk/stm_countvn`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      gettoperrorcode(info){
        const _url = `${this.apiUrl}/hicm-op-ucs/toperrorcode`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }  
  }
