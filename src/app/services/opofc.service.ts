import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class OpofcService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
    
      getselect_debt_account(){
        const _url = `${this.apiUrl}/hicm-op-bkk/select_debt_account`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getselect_debt_account_hi(){
        const _url = `${this.apiUrl}/hicm-op-bkk/select_debt_account_hi`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getopofcnull(info) {
        const _url = `${this.apiUrl}/hicm-op-ofc/opofcnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopofcaccnotnull(info) {
        const _url = `${this.apiUrl}/hicm-op-ofc/opofcaccnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopofcaccnull(info) {
        const _url = `${this.apiUrl}/hicm-op-ofc/opofcaccnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopofcnotnull(info) {
        const _url = `${this.apiUrl}/hicm-op-ofc/opofcnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopofcaccbydate(info) {
        const _url = `${this.apiUrl}/hicm-op-ofc/opofcaccbydate`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopofc(info){
        const _url = `${this.apiUrl}/hicm-op-ofc/stm_op_ofc`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmopofc_stmno(info){
        const _url = `${this.apiUrl}/hicm-op-ofc/stm_op_ofc_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopofc_detail(info){
        const _url = `${this.apiUrl}/hicm-op-ofc/stm_op_ofc_detail`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopofc_sum(info){
        const _url = `${this.apiUrl}/hicm-op-ofc/stm_op_ofc_sum`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmopofc_sum_stmno(info){
        const _url = `${this.apiUrl}/hicm-op-ofc/stm_op_ofc_sum_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }      
      getstmopCount(info){
        const _url = `${this.apiUrl}/hicm-op-ofc/stm_countvn`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      } 
      gettoperrorcode(info){
        const _url = `${this.apiUrl}/hicm-op-ofc/toperrorcode`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }             
  }
