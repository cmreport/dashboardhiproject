import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReportpwardpttypeService {
  accessToken: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    //สร้างตัวแปร accessToken รับค่า accessToken
    this.accessToken = sessionStorage.getItem('accessToken');
    this.httpOptions = {
      headers: new HttpHeaders({

        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' +this.accessToken
      })
    };
  }

    async select() {
      const _url = `${this.apiUrl}/productivity/select`;
      return this.httpClient.get(_url, this.httpOptions).toPromise();
   }

    async selete_date(info: any) {
      const _url = `${this.apiUrl}/productivity/select_date`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    async selete_date_code(info: any) {
        const _url = `${this.apiUrl}/productivity/select_date_code`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

    async select_dprtm(info: any){
        const _url = `${this.apiUrl}/productivity/select_dprtm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }

    async selete_pd_pttype(info: any) {
        const _url = `${this.apiUrl}/productivity/select_pd_pttype`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
}
